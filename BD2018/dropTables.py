import psycopg2
import sys

def drop_tables():
    """ drop tables in the PostgreSQL database"""

    try:
        connection = psycopg2.connect(host="localhost",database="DropMusic", user="postgres", password="postgres")
        cursor = connection.cursor()
        sql_file = open('PostgreSQL/drop_tables.pgsql', 'r')
        cursor.execute(sql_file.read())

        # close communication with the PostgreSQL database server
        cursor.close()
        # commit the changes
        connection.commit()


    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()


if __name__ == '__main__':
    drop_tables()
    input("press enter to continue")
