




from classSong import c_songElement


class c_albumElement():

    def __init__(self):
        self.id = None
        self.name  = None
        self.albumCreationDate = None
        self.description = None
        self.photo = None
        self.photoFilename = None
        self.publisher = None
        self.genres = []
        self.listArtists = []
        self.rating = None



    def getDetails(self, details):
        #assumes: ID, photo, name, genres, description, publisher, artists, release date, rating,
        self.id = details[0]
        self.photo = details[1]
        self.name = details[2]
        genres = details[3].split('/')
        for g in genres:
            self.genres.append(g)
        self.description = details[4]
        self.publisher = details[5]
        artists = details[6].split('/')
        for a in artists:
            self.listArtists.append(a)
        self.albumCreationDate = details[7]
        self.rating = details[8]

        pass

    def getGeneral(self, general):
        #assumes: ID, photo, name, artists, rating
        self.id = general[0]
        self.photo = general[1]
        self.name = general[2]
        artists = general[3].split('/')
        for a in artists:
            self.listArtists.append(a)
        self.rating = general[4]


    def printDetails(self):
        toprint = ("ID:", self.id, '|Name:', self.name, '|Artists:', self.listArtists,
              "|Album release date", self.albumCreationDate, "|Description:", self.description,
              "|Publisher:", self.publisher,
              "|Genres:", self.genres, "|Rating:", self.rating)
        print(toprint)
        return toprint

    def decodeDetails(self):
        #toprint = ("ID:", self.id, '\nName:', self.name, '\nArtists:', self.listArtists,
         #     "\nAlbum release date", self.albumCreationDate, "\nDescription:", self.description,
         #     "\nPublisher:", self.publisher,
         #     "\nGenres:", self.genres, "\nRating:", self.rating)

        stringArtist = self.listArtists[0]
        for x in self.listArtists[1:]:
            stringArtist += "\n " + x

        stringGenres =  self.genres[0]
        for x in self.genres[1:]:
            stringGenres+= "\n " + x
        toprint = "ID: \n {}\n\n" \
                  "Name: \n {}\n\n" \
                  "Artists: \n {}\n\n" \
                  "Album release date: \n {}\n\n" \
                  "Publisher: \n {}\n\n" \
                  "Genres: \n {}\n\n" \
                  "Rating: \n {}\n\n" \
                  "Description: \n {}".format(self.id, self.name, stringArtist, self.albumCreationDate, self.publisher, stringGenres, self.rating, self.description)
        return toprint

    def printGeneral(self):
        print("ID:", self.id, ',Name:', self.name, ',Artists:', self.listArtists, "|Rating:", self.rating)

    def decodeGeneral(self):
        #toprint = ("ID:", self.id, ',Name:', self.name, ',Artists:', self.listArtists, "|Rating:", self.rating)

        stringArtist = ""
        stringArtist = self.listArtists[0]
        for x in self.listArtists[1:]:
            stringArtist += ", " + x


        toprint = "{:7}|{:30}|{:30}|{:5}".format(self.id, self.name, stringArtist, self.rating)

        return toprint

    def headerGeneral(self):
        toPrint = "{:7}|{:30}|{:30}|{:5}".format("ID", "Name", "Artists", "Rating")
        return toPrint

    def getID(self):
        return self.id

class c_album():

    def __init__(self):


        #order: album name, publisher name
        self.sqlInsertAlbum = """INSERT INTO albums (name, publishers_id)
                            SELECT (%s), publishers.id
                            FROM publishers
                            WHERE publishers.name = (%s)
                            RETURNING albums.id;"""
        #order: artist name, album id,
        self.slqLinkToArtist ="""INSERT INTO artist_albums (artist_id, albums_id)
                            SELECT artist.id, albums.id
                            FROM artist, albums
                            WHERE artist.name = (%s) and albums.id = (%s);"""


        #order: album id, genre name,
        self.sqlLinktoGenre ="""INSERT INTO genres_albums (genres_name, albums_id)
                            SELECT genres.name, (%s)
                            FROM genres
                            WHERE genres.name = (%s);"""

        #order: date, album id
        self.sqlUpdateCreationDate = """UPDATE albums
                                        SET albumcreationdate = (%s)
                                        WHERE albums.id = (%s);"""
        #order: description, album id
        self.sqlUpdateDescription = """UPDATE albums
                                        SET description = (%s)
                                        WHERE albums.id = (%s)"""
        #order:  album id
        self.sqlEraseAlbum = """DELETE
                                FROM albums
                                WHERE albums.id = (%s)"""


        #order: album id
        self.sqlGetAlbums = """SELECT  al.id,
                                        al.photo AS "Artwork",
                                        al.name AS "Name",
                                        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
                                        'Stars' AS "Rating"
                                FROM    albums al
                                LEFT JOIN    genres_albums g_al ON g_al.albums_id = al.id
                                LEFT JOIN    genres g ON g_al.genres_name = g.name
                                JOIN    publishers p ON al.publishers_id = p.id
                                JOIN    artist_albums ar_al ON ar_al.albums_id = al.id
                                JOIN    artist ar ON ar_al.artist_id = ar.id
                                GROUP BY al.id,
                                        al.photo,
                                        al.name,
                                        al.description,
                                        p.name,
                                        al.albumcreationdate
                                ORDER BY al.name
                                ;"""

        #oder: album id
        self.sqlGetAlbumInfo = """SELECT  al.id,
                                            al.photo AS "Artwork",
                                            al.name AS "Name",
                                            array_to_string(array_agg(distinct g.name),'/') AS "Genre",
                                            al.description,
                                            p.name AS "Publisher",
                                            array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
                                            al.albumcreationdate, 
                                            'Stars' AS "Rating"
                                    FROM    albums al
                                    LEFT JOIN    genres_albums g_al ON g_al.albums_id = al.id
                                    LEFT JOIN    genres g ON g_al.genres_name = g.name
                                    JOIN    publishers p ON al.publishers_id = p.id
                                    JOIN    artist_albums ar_al ON ar_al.albums_id = al.id
                                    JOIN    artist ar ON ar_al.artist_id = ar.id
                                    WHERE   al.id = (%s)
                                    GROUP BY al.id,
                                            al.photo,
                                            al.name,
                                            al.description,
                                            p.name,
                                            al.albumcreationdate
                                    ORDER BY al.name
                                    ;"""


        self.sqlSearchAlbumsName = """SELECT  al.id,
                                                al.photo AS "Artwork",
                                                al.name AS "Name",
                                                array_to_string(array_agg(distinct g.name),'/') AS "Genre",
                                                al.description,
                                                p.name AS "Publisher",
                                                array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
                                                al.albumcreationdate, 
                                                'Stars' AS "Rating"
                                        FROM    albums al
                                        LEFT JOIN genres_albums g_al ON g_al.albums_id = al.id
                                        LEFT JOIN genres g ON g_al.genres_name = g.name
                                        JOIN    publishers p ON al.publishers_id = p.id
                                        JOIN    artist_albums ar_al ON ar_al.albums_id = al.id
                                        JOIN    artist ar ON ar_al.artist_id = ar.id
                                        WHERE   UPPER(al.name) LIKE UPPER((%s))
                                        GROUP BY al.id,
                                                al.photo,
                                                al.name,
                                                al.description,
                                                p.name,
                                                al.albumcreationdate
                                        ORDER BY al.name
                                        ;"""


        self.sqlGetAlbumSongs = """SELECT  s.id,
                                        s.name AS "Name",
                                        s.duration AS "Duration",
                                        'Stars' AS "Rating"
                                FROM    songs s
                                WHERE   s.albums_id = (%s)
                                ;"""

        #order: description, album id
        self.sqlUpdateAlbumDescription = """UPDATE albums
                                        SET description = (%s)
                                        WHERE id=(%s)"""
        return


    def headerGeneral(self):
        return c_albumElement().headerGeneral()
    def create(self, connection, albumName, publisherName, artistName, genres, albumCreationDate = None, description = None):

        if connection == None or albumName == None or publisherName == None or artistName == None or genres == None:
            return -1



        print("createAlbum: inserting album")
        cursor = connection.cursor()
        cursor.execute(self.sqlInsertAlbum, (albumName, publisherName))
        id = cursor.fetchone()

        print("createAlbum: inserting album to artist connection")
        if type(artistName) == list:
            for x in artistName:
               cursor.execute(self.slqLinkToArtist, (x, id))
        else:
            cursor.execute(self.slqLinkToArtist, (artistName, id))

        print("createAlbum: inserting genre to album connection")
        if type(genres) == list:
            for x in genres:
                print("inserting genre:", x)
                cursor.execute(self.sqlLinktoGenre, (id, x))
        else:
            cursor.execute(self.sqlLinktoGenre, (id, x))


        if albumCreationDate:
            print("createAlbum: updating album creation date")
            cursor.execute(self.sqlUpdateCreationDate, (albumCreationDate, id))
            pass
        if description:
            print("createAlbum: updating album description")
            cursor.execute(self.sqlUpdateDescription, (description, id))
            pass


        connection.commit()


        cursor.close()


        return 0


    def erase(self,connection, id):

        print("erase: atemping to erase album:", id)
        cursor = connection.cursor()
        cursor.execute(self.sqlEraseAlbum, [id])

        connection.commit()


        cursor.close()



    def getListOfAlbums(self, connection):
        print("Getting list of albums")
        #listAlbums = []
        cursor = connection.cursor()
        cursor.execute(self.sqlGetAlbums)


        listAlbums = cursor.fetchall()
        connection.commit()


        listOfAlbums = []

        for x in listAlbums:
            newElement = c_albumElement()
            newElement.getGeneral(x)
            listOfAlbums.append(newElement)


        cursor.close()

        return listOfAlbums

    def getAlbumInfo(self, connection, id):
        print("Getting album info")
        cursor = connection.cursor()
        cursor.execute(self.sqlGetAlbumInfo, [id] )


        albumInfo = cursor.fetchall()
        connection.commit()
        #print(albumInfo)

        newElement = None
        #listOfAlbums = []

        for x in albumInfo:
            newElement = c_albumElement()
            newElement.getDetails(x)
            #listOfAlbums.append(newElement)



        cursor.close()

        newElement.printDetails()
        return newElement



    def changeDescription(self,connection, id, description):
        print("Updating album description")
        cursor = connection.cursor()
        cursor.execute(self.sqlUpdateAlbumDescription, (description, id) )


        connection.commit()
        cursor.close()

        pass

    def addGenres(self,connection,id, genre):
        sqladdGenre = """insert into genres_albums(genres_name, albums_id)
                            select genres.name, albums.id
                            from genres, albums
                            where genres.name= (%s) and albums.id = (%s)"""

        sqlCheckGenre = """select* from genres_albums where genres_name = (%s) and albums_id = (%s)"""

        cursor = connection.cursor()

        cursor.execute(sqlCheckGenre, (genre, id) )
        genresTable = cursor.fetchall()
        print(genresTable)
        if genresTable:
            print("Album already has that genre")
        else:

            try:
                cursor.execute(sqladdGenre, (genre, id))
            except(Exception, psycopg2.IntegrityError) as error:
                print("Exception", error)

        connection.commit()
        cursor.close()
        pass
    def deleteGenres(self, connection, id, genres):

        #order: genre name, album id
        sqlDeleteGenre = """DELETE
                            FROM genres_albums
                            WHERE genres_name= (%s) and albums_id = (%s)"""

        cursor = connection.cursor()
        cursor.execute(sqlDeleteGenre, (genre, id))
        connection.commit()
        cursor.close()
        pass


    def changePhoto(self,id, photo):
        pass

    def searchAlbumByName(self, connection, name):
        print("search list of albums")
        encodedName = '%'+name+'%'
        print(encodedName)
        #listAlbums = []
        cursor = connection.cursor()
        cursor.execute(self.sqlSearchAlbumsName, [encodedName])

        listAlbums = cursor.fetchall()

        connection.commit()


        listOfAlbums = []

        for x in listAlbums:
            newElement = c_albumElement()
            newElement.id = x[0]
            newElement.photo = x[1]
            newElement.name = x[2]
            genres = x[3].split('/')
            for g in genres:
                newElement.genres.append(g)
            newElement.description = x[4]
            newElement.publisher = x[5]
            artists = x[6].split('/')
            for a in artists:
                newElement.listArtists.append(a)
            newElement.albumCreationDate = x[7]
            newElement.rating = x[8]
            listOfAlbums.append(newElement)

        cursor.close()

        return listOfAlbums


    def getSongList(self, connection, id):
        print("Getting album list of songs")
        cursor = connection.cursor()
        cursor.execute(self.sqlGetAlbumSongs, [id] )


        listSongs = cursor.fetchall()

        listOfSongs = []
        for x in listSongs:
            newElement = c_songElement()
            newElement.getInAlbum(x)
            listOfSongs.append(newElement)
        connection.commit()

        cursor.close()
        for x in listOfSongs:
            x.printInAlbum()

        return listOfSongs
