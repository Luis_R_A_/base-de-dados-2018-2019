from classSong import c_songElement

class c_playlistElement():

    def __init__(self):
        self.id = None
        self.name  = None
        self.description = None
        self.playlistCreationDate = None
        self.private = None
        self.owner = None


    def getDetails(self, details):
        #assumes: ID, name, description, playlistcreationdate, private, owner
        self.id = details[0]
        self.name = details[1]
        self.description = details[2]
        self.playlistCreationDate = details[3]
        self.private = details[4]
        self.owner  = details[5]

    def getGeneral(self, general):
        #get id, name, playlistCreationDate, private
        self.id = general[0]
        self.name = general[1]
        self.playlistCreationDate = general[2]
        self.Private = general[3]

    def decodeDetails(self):
        yn = ['No', 'Yes']
        toprint = "ID: \n {}\n\n" \
                  "Name: \n {}\n\n" \
                  "Playlist Creation Date: \n {}\n\n" \
                  "Owner: \n {} \n\n" \
                  "Private: \n {}\n\n" \
                  "Description: \n {}".format(self.id, self.name, self.playlistCreationDate,  self.owner, yn[self.private], self.description)
        return toprint

    def printGeneral(self):
        yn = ['No', 'Yes']
        toprint = "{:7}|{:30}|{:30}|{:8}".format(self.id, self.name, self.playlistCreationDate.strftime("%Y-%m-%d"), yn[self.Private])
        print(toprint)

    def decodeGeneral(self):
        yn = ['No', 'Yes']
        toprint = "{:7}|{:30}|{:30}|{:8}".format(self.id, self.name, self.playlistCreationDate.strftime("%Y-%m-%d"), yn[self.Private])
        return toprint

    def headerGeneral(self):
        toPrint = "{:7}|{:30}|{:30}|{:8}".format("ID", "Name", "C.Date", "Private")
        return toPrint

    def getID(self):
        return self.id



class c_playlist():

    def __init__(self):

        self.sqlGetMyPlaylists = """SELECT  pl.id,
                                            pl.name         AS "Name",
                                            pl.playlistcreationdate,
                                            pl.private
                                    FROM    playlists pl
                                    JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
                                    JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
                                    WHERE   (u.emaillogin = (%s))
                                    ;"""
        self.sqlSearcMyPlaylistsByName = """SELECT  pl.id,
                                            pl.name         AS "Name",
                                            pl.playlistcreationdate,
                                            pl.private
                                    FROM    playlists pl
                                    JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
                                    JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
                                    WHERE   (u.emaillogin = (%s)) AND
                                            LOWER(pl.name) LIKE LOWER((%s))
                                    ;"""
        #order: name, description, userID, playlist id
        self.sqlCreatePlaylist = """INSERT  INTO playlists (name, description, playlistcreationdate, private) 
                                    VALUES ((%s), (%s), CURRENT_DATE, TRUE) RETURNING id;"""
        #order userID, playlist id
        self.sqlLinkPlaylistToUser = """INSERT INTO users_playlists(users_emaillogin, playlists_id)
                                        VALUES((%s), (%s));"""
        #order: playlist ID
        self.sqlErasePlaylist = """DELETE
                                FROM playlists
                                WHERE id = (%s)"""

        #order: id
        self.sqlGetPlaylistInfo = """SELECT  pl.id,
                                                pl.name                 AS "Name",
                                                pl.description          AS "Description",
                                                pl.playlistcreationdate AS "Creation Date",
                                                pl.private,
                                                u.emaillogin            AS "Owner"
                                        FROM    playlists pl
                                        JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
                                        JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
                                        WHERE   pl.id = (%s)"""

        #order: id
        self.sqlGetPlaylistSongs = """SELECT  s.id,
                                            s.name          AS "Name",
                                            al.name         AS "Album",
                                            s.duration      AS "Duration",
                                            'Stars'   AS "Rating"
                                    FROM    songs s
                                    LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
                                    LEFT JOIN       comments c              ON s_c.comments_id = c.id
                                    JOIN            albums al               ON al.id = s.albums_id
                                    JOIN            playlists_songs pl_s    ON pl_s.songs_id = s.id
                                    JOIN            playlists pl            ON pl_s.playlists_id = pl.id
                                    WHERE           pl.id = (%s)
                                    GROUP BY        s.id,
                                                    al.name
                                    ;"""

        #order: playlist id, song id
        self.sqlAddSongToPlaylist = """INSERT  INTO playlists_songs (playlists_id, songs_id)
                                        VALUES ((%s), (%s))"""
        #order: playlist id, song id
        self.sqlCheckIfSongInPlaylist = """SELECT * FROM playlists_songs WHERE playlists_id = (%s) and songs_id = (%s)"""

        #order: playlist id, song id
        self.sqlRemoveSongFromPlaylist = """DELETE FROM playlists_songs WHERE playlists_id = (%s) and songs_id = (%s)"""

        #order: new playlist name, playlist id
        self.sqlUpdatePlaylistName = """UPDATE  playlists
                                        SET     name = (%s)
                                        WHERE   id = (%s)"""
        #order: description, playlistID
        self.sqlUpdatePlaylistDescription = """UPDATE  playlists
                                                SET     description = (%s)
                                                WHERE   id = (%s)"""
        #order: playlist id
        self.sqlRemovePlaylist = """DELETE
                                    FROM    playlists
                                    WHERE   id = (%s)"""
        pass

    def removePlaylist(self, connection, playlistID):
        print("removing playlist")

        cursor = connection.cursor()
        cursor.execute(self.sqlRemovePlaylist, [playlistID])

        #playlistID = cursor.fetchall()
        #if playlistID:
        #    cursor.execute(self.sqlLinkPlaylistToUser, [userID, playlistID[0]])
        connection.commit()

        cursor.close()


    def updateName(self, connection, playlistID, name):
        print("updating playlist name")
        cursor = connection.cursor()
        cursor.execute(self.sqlUpdatePlaylistName, [name, playlistID] )
        connection.commit()
        cursor.close()

    def updateDescription(self, connection, playlistID, description):
        print("updating playlist name")
        cursor = connection.cursor()
        cursor.execute(self.sqlUpdatePlaylistDescription, [description, playlistID ] )
        connection.commit()
        cursor.close()

    def getSongList(self, connection, playlistID):
        print("Getting album list of songs")
        cursor = connection.cursor()
        cursor.execute(self.sqlGetPlaylistSongs, [playlistID] )


        listSongs = cursor.fetchall()

        listOfSongs = []
        for x in listSongs:
            newElement = c_songElement()
            newElement.getGeneral(x)
            listOfSongs.append(newElement)
        connection.commit()

        cursor.close()
        for x in listOfSongs:
            x.printInAlbum()

        return listOfSongs

    def getPlaylistDetails(self, connection, id):
        print("Getting album info")
        cursor = connection.cursor()
        cursor.execute(self.sqlGetPlaylistInfo, [id] )


        playlistInfo = cursor.fetchall()
        print(playlistInfo)
        connection.commit()
        #print(albumInfo)

        newElement = None
        #listOfAlbums = []

        for x in playlistInfo:
            newElement = c_playlistElement()
            newElement.getDetails(x)
            #listOfAlbums.append(newElement)



        cursor.close()

        return newElement

    def headerGeneral(self):
        return c_playlistElement().headerGeneral()

    def getListOfMyPlaylists(self, connection, userId):
        print("Getting list of playlists")
        #listAlbums = []
        cursor = connection.cursor()
        cursor.execute(self.sqlGetMyPlaylists, [userId])


        listPlaylists = cursor.fetchall()
        connection.commit()


        listOfPlaylists = []

        for x in listPlaylists:
            newElement = c_playlistElement()
            newElement.getGeneral(x)
            listOfPlaylists.append(newElement)


        cursor.close()

        return listOfPlaylists

    def getListOfMyPlaylistsByName(self, connection, userId, searchKey):
        print("Getting list of playlists")

        encodedName = '%'+searchKey+'%'
        #listAlbums = []
        cursor = connection.cursor()
        cursor.execute(self.sqlSearcMyPlaylistsByName, [userId,encodedName])


        listPlaylists = cursor.fetchall()
        connection.commit()


        listOfPlaylists = []

        for x in listPlaylists:
            newElement = c_playlistElement()
            newElement.getGeneral(x)
            listOfPlaylists.append(newElement)


        cursor.close()

        return listOfPlaylists

    #always private when created
    def create(self, connection, userID, name, description):

        print("Creating playlist")


        cursor = connection.cursor()
        cursor.execute(self.sqlCreatePlaylist, [name, description])

        playlistID = cursor.fetchall()
        if playlistID:
            cursor.execute(self.sqlLinkPlaylistToUser, [userID, playlistID[0]])
            connection.commit()

        cursor.close()

    def erase(self, connection, playlistID):
        print("erase: atemping to erase playlist:", playlistID)
        cursor = connection.cursor()
        cursor.execute(self.sqlErasePlaylist, (playlistID))

        connection.commit()


        cursor.close()

    def checkIfSongInPlaylist(self, connection, playListID, songID):
        print("checking if song in playlist")

        cursor = connection.cursor()

        cursor.execute(self.sqlCheckIfSongInPlaylist, [playListID, songID])


        listPlaylists = cursor.fetchall()
        connection.commit()




        cursor.close()

        if listPlaylists:
            return True
        else:
            return False

        return None
    def addSong(self, connection, playListID, songID):
        print("Adding song to playlist")

        check = self.checkIfSongInPlaylist(connection, playListID, songID)
        if check:
            return
        cursor = connection.cursor()
        try:
            cursor.execute(self.sqlAddSongToPlaylist, [playListID, songID])


            connection.commit()
        except:
            print("error")



        cursor.close()

        return None

    def removeSong(self, connection, playListID, songID):
        print("Removing song from playlist")

        check = self.checkIfSongInPlaylist(connection, playListID, songID)
        if not check:
            return

        cursor = connection.cursor()
        try:
            cursor.execute(self.sqlRemoveSongFromPlaylist, [playListID, songID])


            connection.commit()
        except:
            print("error")
