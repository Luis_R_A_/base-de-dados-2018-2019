import tkinter as tk


class BarDalho(tk.Tk):

    def __init__(self, *args, **kwargs):
        
        tk.Tk.__init__(self, *args, **kwargs)
        container = tk.Frame(self)

        container.pack(side="top", fill="both", expand = True)

        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}

        for F in (StartPage, SearchPage):

            frame = F(container, self)

            self.frames[F] = frame

            frame.grid(row=0, column=0, sticky="nsew")

        self.show_frame(StartPage)

    def show_frame(self, cont):

        frame = self.frames[cont]
        frame.tkraise()

        
class StartPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self,parent)
        search_button = tk.Button(self, text="Search",
                                  command=lambda: controller.show_frame(SearchPage))
        search_button.grid(column=0, row=0, columnspan=1, rowspan=1)

        info_label = tk.Label(self, text="Start Page")
        info_label.grid(column=1, row=0, columnspan=200, rowspan=200)


class SearchPage(tk.Frame):

    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        search_button = tk.Button(self, text="Search",
                                  command=lambda: controller.show_frame(SearchPage))
        search_button.grid(column=0, row=0, columnspan=1, rowspan=1)

        button1 = tk.Button(self, text="button1",
                                command=lambda: controller.show_frame(StartPage))
        button1.grid(column=0, row=1, columnspan=1, rowspan=1)

        button2 = tk.Button(self, text="button2",
                                command=lambda: controller.show_frame(StartPage))
        button2.grid(column=0, row=2, columnspan=1, rowspan=1)

        button3 = tk.Button(self, text="button3",
                                command=lambda: controller.show_frame(StartPage))
        button3.grid(column=0, row=3, columnspan=1, rowspan=1)

        back_button = tk.Button(self, text="Back",
                                command=lambda: controller.show_frame(StartPage))
        back_button.grid(column=0, row=4, columnspan=1, rowspan=1)

        result_label = tk.Label(self, text="SQL Results")
        result_label.grid(column=1, row=0, columnspan=200, rowspan=200)



app = BarDalho()
app.mainloop()
