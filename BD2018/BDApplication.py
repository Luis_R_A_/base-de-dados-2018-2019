
from tkinter import *
import tkinter as tk
from tkinter import ttk

import psycopg2



from classLogin import c_login
from classPlaylist import c_playlist
from classAlbum import c_album
from classSong import c_song
import datetime

#=======================================
# Parent class for windows created after the first main window
#=======================================
class c_genericChildWindow():

    def __init__(self, topLevel, hideTopLevel):
        self.topLevel = topLevel
        self.root = tk.Toplevel()
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.windowOpen = True
        self.topLevelHid = hideTopLevel
        if(hideTopLevel):
            self.topLevel.withdraw()
        self.root.resizable(False, False)

    def stillOpen(self):
        return self.windowOpen

    def on_closing(self):
        self.windowOpen = False
        if self.topLevelHid:
            self.topLevel.deiconify()
        self.root.destroy()

    def close(self):
        print("Closing window")
        self.windowOpen = False
        if self.topLevelHid:
            self.topLevel.deiconify()
        self.root.destroy()




#**********************************************************************************************
#
# Album related windows
#
#**********************************************************************************************
class c_openAlbum(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject, id):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Open album window")
        self.loginObject = loginObject
        self.albumID = id




        self.tfieldAlbum = tk.Text(self.root, width = 30, height = 20)
        self.tfieldAlbum.grid(row = 1, column = 3, rowspan=35, columnspan = 3,sticky = N+S+E+W)

        self.labelSearch = tk.Label(self.root,  text = "List of Songs", font=("Arial", 12))
        self.labelSearch.grid(row = 0, column = 7, columnspan=2, sticky = N+S+E+W)
        self.tfieldSongsHeader = tk.Text(self.root, width = 70, height = 1)
        self.tfieldSongsHeader.grid(row = 1, column = 7, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.tfieldAlbumSongs = tk.Text(self.root, width = 70, height = 35)
        self.tfieldAlbumSongs.grid(row = 2, column = 7, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.songObject = c_song()
        self.tfieldSongsHeader.insert("end",self.songObject.headerInAlbum())
        self.tfieldSongsHeader.insert("end","\n")

        self.albumObject = c_album()
        self.albumInfo = self.albumObject.getAlbumInfo(self.loginObject.connection, id)
        albumText = self.albumInfo.decodeDetails()
        self.tfieldAlbum.insert(1.0, albumText)
        self.tfieldAlbum.config(state=DISABLED)



        self.songList = self.albumObject.getSongList(self.loginObject.connection, id)
        listOfIDandSongs = []
        self.dictionarySongs = {}
        for x in self.songList:
            entry = "{}: {}".format(x.getID(), x.name)
            listOfIDandSongs.append(entry)
            self.dictionarySongs[entry] = x.getID()
            self.tfieldAlbumSongs.insert("end",x.decodeInAlbum())
            self.tfieldAlbumSongs.insert("end","\n")
        self.tfieldAlbumSongs.config(state=DISABLED)


        self.comboBoxOpen = ttk.Combobox(self.root, values=listOfIDandSongs, state="readonly", height=4)
        self.comboBoxOpen.grid(row = 15, column = 0, columnspan=2, sticky = N+S+E+W)
        self.buttonOpen= ttk.Button(self.root,text="Open song", command=self.open)
        self.buttonOpen.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

    def open(self):
        print("Pressed open")
        entry = self.comboBoxOpen.get()
        if entry =='':
            return
        songID = self.dictionarySongs[entry]
        if id:
            c_openSong(self.root, True, self.loginObject, songID)

class c_searchAlbum(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Search album window")
        self.loginObject = loginObject

        self.labelSearch = tk.Label(self.root,  text = "Search by name", font=("Arial", 12))
        self.labelSearch.grid(row = 0, column = 1, columnspan=2, sticky = N+S+E+W)
        self.entrySearch = tk.Entry(self.root, width = 50)
        self.entrySearch.grid(row = 2, column = 1, columnspan=3, sticky = N+S+E+W)
        self.buttonRegister = ttk.Button(self.root,text="Search", command=self.search)
        self.buttonRegister.grid(row = 2, column = 0, rowspan=1, sticky = N+S+E+W)

        #self.entryOpen = tk.Entry(self.root, width = 50)
        #self.entryOpen.grid(row = 16, column = 1, columnspan=4, sticky = N+S+E+W)
        self.comboBoxOpen = ttk.Combobox(self.root, values=[], state="readonly", height=4)
        self.comboBoxOpen.grid(row = 16, column = 1, columnspan=4, sticky = N+S+E+W)
        self.buttonOpen= ttk.Button(self.root,text="Open", command=self.open)
        self.buttonOpen.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        ttk.Separator(self.root, orient=HORIZONTAL).grid( row=15, column=0, columnspan=10, sticky='we')


        self.tfieldAlbums = tk.Text(self.root, width = 110, height = 35)
        self.tfieldAlbums.grid(row = 1, column = 5, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.tfieldAlbums.config(state=DISABLED)

        self.tfieldAlbumsHeader = tk.Text(self.root, width = 110, height = 1)
        self.tfieldAlbumsHeader.grid(row = 0, column = 5, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.albumObject = c_album()
        self.tfieldAlbumsHeader.insert("end",self.albumObject.headerGeneral())
        self.tfieldAlbumsHeader.insert("end","\n")

        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldAlbums.yview)
        self.scrollb.grid(row=1, column=6+3, rowspan = 35,sticky='nsew')
        self.tfieldAlbums['yscrollcommand'] = self.scrollb.set
        self.updateAlbums()

    def updateAlbums(self):
        self.search()

    def search(self):
        print("pressed search")
        self.tfieldAlbums.config(state=NORMAL)
        self.tfieldAlbums.delete(1.0,END)
        searchKey = self.entrySearch.get()
        listOfAlbums = []
        if searchKey == '':
            listOfAlbums = self.albumObject.getListOfAlbums(self.loginObject.connection)
        else:
            listOfAlbums = self.albumObject.searchAlbumByName(self.loginObject.connection, searchKey)
        listOfIDandAlbums = []
        self.dictionaryAlbums = {}
        for x in listOfAlbums:
            entry = "{}: {}".format(x.getID(), x.name)
            listOfIDandAlbums.append(entry)
            self.dictionaryAlbums[entry] = x.getID()
            self.tfieldAlbums.insert("end",x.decodeGeneral())
            self.tfieldAlbums.insert("end","\n")
        self.tfieldAlbums.config(state=DISABLED)
        self.comboBoxOpen.set([])
        self.comboBoxOpen['values'] = listOfIDandAlbums

    def open(self):
        print("pressed open")
        entry = self.comboBoxOpen.get()
        if entry == '':
            return
        albumID = self.dictionaryAlbums[entry]
        if id:
            c_openAlbum(self.root, True, self.loginObject, albumID)
 #**********************************************************************************************
# END OF Album related windows
#**********************************************************************************************

#**********************************************************************************************
#
# Song related windows
#
#**********************************************************************************************
class c_editSongLyrics(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject, id):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Edit song lyric window")
        self.loginObject = loginObject
        self.id = id

        self.tfieldLyrics = tk.Text(self.root, width = 70, height = 35)
        self.tfieldLyrics.grid(row = 2, column = 7, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldLyrics.yview)
        self.scrollb.grid(row=2, column=7+3, rowspan = 35,sticky='nsew')
        self.tfieldLyrics['yscrollcommand'] = self.scrollb.set

        self.songObject = c_song()
        self.songInfo = self.songObject.getSongInfo(self.loginObject.connection, id)
        lyrics = self.songInfo.getLyrics()
        self.tfieldLyrics.insert(1.0, lyrics)



        self.buttonAccept= ttk.Button(self.root,text="Accept", command=self.accept)
        self.buttonAccept.grid(row = 14, column = 0, rowspan=1, sticky = N+S+E+W)

        self.buttonCancel= ttk.Button(self.root,text="Cancel", command=self.cancel)
        self.buttonCancel.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)


    def cancel(self):
        self.close()

    def accept(self):
        newLyric = self.tfieldLyrics.get("1.0",END)
        self.songObject.insertLyrics(self.loginObject.connection, self.id, newLyric)
        self.close()

class c_openSong(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject, id):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Open song window")
        self.loginObject = loginObject

        self.id = id


        self.tfieldSong = tk.Text(self.root, width = 30, height = 20)
        self.tfieldSong.grid(row = 1, column = 3, rowspan=35, columnspan = 3,sticky = N+S+E+W)

        self.labelSearch = tk.Label(self.root,  text = "Lyrics", font=("Arial", 12))
        self.labelSearch.grid(row = 0, column = 7, columnspan=2, sticky = N+S+E+W)
        #self.tfieldSongsHeader = tk.Text(self.root, width = 70, height = 1)
        #self.tfieldSongsHeader.grid(row = 1, column = 7, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.tfieldLyrics = tk.Text(self.root, width = 70, height = 35)
        self.tfieldLyrics.grid(row = 2, column = 7, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldLyrics.yview)
        self.scrollb.grid(row=2, column=7+3, rowspan = 35,sticky='nsew')
        self.tfieldLyrics['yscrollcommand'] = self.scrollb.set

        self.songObject = c_song()
        self.songInfo = self.songObject.getSongInfo(self.loginObject.connection, id)
        songText = self.songInfo.decodeDetails()
        self.tfieldSong.insert(1.0, songText)
        self.tfieldSong.config(state=DISABLED)

        lyrics = self.songInfo.getLyrics()
        self.tfieldLyrics.insert(1.0, lyrics)
        self.tfieldLyrics.config(state=DISABLED)

        if self.loginObject.sessionIsAdmin:
            self.buttonEdit = self.buttonOpen= ttk.Button(self.root,text="Edit", command=self.edit)
            self.buttonEdit.grid(row = 10, column = 0, rowspan=1, sticky = N+S+E+W)
            editOptions = ['Lyrics']
            self.comboBoxEdit = ttk.Combobox(self.root, values=editOptions, state="readonly", height=4)
            self.comboBoxEdit.grid(row = 9, column = 0, columnspan=1, sticky = N+S+E+W)
            self.comboBoxEdit.set(editOptions[0])

        self.labelPlaylist = tk.Label(self.root,  text = "Add to Playlist", font=("Arial", 16))
        self.labelPlaylist.grid(row = 14, column = 0, rowspan=1, sticky = N+S+E+W)
        self.comboBoxPlaylists = ttk.Combobox(self.root, values=[], state="readonly", height=4)
        self.comboBoxPlaylists.grid(row = 15, column = 0, columnspan=2, sticky = N+S+E+W)
        self.buttonAdd= ttk.Button(self.root,text="Add", command=self.addToPlaylist)
        self.buttonAdd.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        playlistObject = c_playlist()
        listOfPlaylist = playlistObject.getListOfMyPlaylists(self.loginObject.connection, self.loginObject.currentEmail)
        listOfIDs = []
        listOfPlaylistNames=[]
        self.dictionaryPlaylist = {}
        for x in listOfPlaylist:
            listOfIDs.append(x.getID())
            entry = "{}: {}".format(x.getID(), x.name)
            listOfPlaylistNames.append(entry)
            self.dictionaryPlaylist[entry] = x.getID()
        self.comboBoxPlaylists['values'] = (listOfPlaylistNames)


    def addToPlaylist(self):
        print("pressed addToPlaylist")
        songID = self.id
        playlistName = self.comboBoxPlaylists.get()
        if playlistName == '':
            return
        playlistID = self.dictionaryPlaylist[playlistName]

        playlistObject = c_playlist()

        check = playlistObject.checkIfSongInPlaylist(self.loginObject.connection, playlistID, songID)

        if check:
            print("Song already in playlist")
        else:
            playlistObject.addSong(self.loginObject.connection, playlistID, songID)




    def updateLyricsText(self):
        self.songInfo = self.songObject.getSongInfo(self.loginObject.connection, self.id)
        self.tfieldLyrics.config(state=NORMAL)
        lyrics = self.songInfo.getLyrics()
        self.tfieldLyrics.delete(1.0, "end")
        self.tfieldLyrics.insert(1.0, lyrics)
        self.tfieldLyrics.config(state=DISABLED)

    def edit(self):
        print("pressed edit")
        option = self.comboBoxEdit.get()
        if option == 'Lyrics':
            editWindow = c_editSongLyrics(self.root, True, self.loginObject, self.id)
            self.root.wait_window(editWindow.root)
            self.updateLyricsText()

class c_searchMusic(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Search music window")
        self.loginObject = loginObject

        self.labelSearch = tk.Label(self.root,  text = "Search by ", font=("Arial", 12))
        self.labelSearch.grid(row = 0, column = 0, columnspan=1, sticky = N+S+E+W)

        searchParameters = ['Song Name', 'Artist Name', 'Song Genre', 'Release Date']
        self.comboBoxSearch = ttk.Combobox(self.root, values=searchParameters, state="readonly", height=4)
        self.comboBoxSearch.grid(row = 0, column = 1, columnspan=1, sticky = N+S+E+W)
        self.comboBoxSearch.set(searchParameters[0])
        self.comboBoxSearch.bind("<<ComboboxSelected>>", self.changeParameter)

        self.defaultMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
        self.defaultDaysMax = {'January':31, 'February':28, 'March':31, 'April':30, 'May':31, 'June':30, 'July':31,
                                'August':31, 'September':30, 'October':31, 'November':30, 'December':31}
        self.defaultDays = list(range(1, 31+1))
        self.labelMin = tk.Label(self.root,  text = "Min Date", font=("Arial", 12))
        self.labelMin.grid(row = 3, column = 0, columnspan=1, sticky = N+S+E+W)
        self.comboBoxMinDay= ttk.Combobox(self.root, values=self.defaultDays, state="readonly", height=4)
        self.comboBoxMinDay.grid(row = 4, column = 2, columnspan=1, sticky = E+W)
        self.comboBoxMinDay.set(self.defaultDays[0])
        self.comboBoxMinMonth= ttk.Combobox(self.root, values=self.defaultMonths, state="readonly", height=4)
        self.comboBoxMinMonth.grid(row = 4, column = 1, columnspan=1, sticky = E+W)
        self.comboBoxMinMonth.set(self.defaultMonths[0])
        self.comboBoxMinYear= ttk.Combobox(self.root, values=[2018], state="readonly", height=4)
        self.comboBoxMinYear.grid(row = 4, column = 0, columnspan=1, sticky = E+W)

        self.comboBoxMinDay.bind("<<ComboboxSelected>>", self.changeParameterMinDay)
        self.comboBoxMinMonth.bind("<<ComboboxSelected>>", self.changeParameterMinMonth)
        self.comboBoxMinYear.bind("<<ComboboxSelected>>", self.changeParameterMinYear)

        self.labelMin.grid_remove()
        self.comboBoxMinDay.grid_remove()
        self.comboBoxMinMonth.grid_remove()
        self.comboBoxMinYear.grid_remove()

        self.labelMax= tk.Label(self.root,  text = "Max Date", font=("Arial", 12))
        self.labelMax.grid(row = 5, column = 0, columnspan=1, sticky = N+S+E+W)
        self.comboBoxMaxDay= ttk.Combobox(self.root, values=self.defaultDays, state="readonly", height=4)
        self.comboBoxMaxDay.grid(row = 6, column = 2, columnspan=1, sticky = E+W)
        self.comboBoxMaxDay.set(self.defaultDays[-1])
        self.comboBoxMaxMonth= ttk.Combobox(self.root, values=self.defaultMonths, state="readonly", height=4)
        self.comboBoxMaxMonth.grid(row = 6, column = 1, columnspan=1, sticky = E+W)
        self.comboBoxMaxMonth.set(self.defaultMonths[-1])
        self.comboBoxMaxYear= ttk.Combobox(self.root, values=[2018], state="readonly", height=4)
        self.comboBoxMaxYear.grid(row = 6, column = 0, columnspan=1, sticky = E+W)

        self.comboBoxMaxDay.bind("<<ComboboxSelected>>", self.changeParameterMaxDay)
        self.comboBoxMaxMonth.bind("<<ComboboxSelected>>", self.changeParameterMaxMonth)
        self.comboBoxMaxYear.bind("<<ComboboxSelected>>", self.changeParameterMaxYear)

        self.labelMax.grid_remove()
        self.comboBoxMaxDay.grid_remove()
        self.comboBoxMaxMonth.grid_remove()
        self.comboBoxMaxYear.grid_remove()

        self.entrySearch = tk.Entry(self.root, width = 50)
        self.entrySearch.grid(row = 2, column = 1, columnspan=1, sticky = N+S+E+W)
        self.buttonRegister = ttk.Button(self.root,text="Search", command=self.search)
        self.buttonRegister.grid(row = 2, column = 0, rowspan=1, sticky = N+S+E+W)

        #self.entryOpen = tk.Entry(self.root, width = 100)
        #self.entryOpen.grid(row = 16, column = 1, columnspan=3, sticky = N+S+E+W)
        self.comboBoxOpen = ttk.Combobox(self.root, values=[], state="readonly", height=4)
        self.comboBoxOpen.grid(row = 16, column = 1, columnspan=1, sticky = N+S+E+W)
        self.buttonOpen= ttk.Button(self.root,text="Open", command=self.open)
        self.buttonOpen.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        ttk.Separator(self.root, orient=HORIZONTAL).grid( row=15, column=0, columnspan=10, sticky='we')


        self.tfieldSongs = tk.Text(self.root, width = 100, height = 35)
        self.tfieldSongs.grid(row = 1, column = 4, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.tfieldSongs.config(state=DISABLED)

        self.tfieldSongsHeader = tk.Text(self.root, width = 100, height = 1)
        self.tfieldSongsHeader.grid(row = 0, column = 4, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.songObject = c_song()
        self.tfieldSongsHeader.insert("end",self.songObject.headerGeneral())
        self.tfieldSongsHeader.insert("end","\n")

        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldSongs.yview)
        self.scrollb.grid(row=1, column=5+3, rowspan = 35,sticky='nsew')
        self.tfieldSongs['yscrollcommand'] = self.scrollb.set

        self.updateSongs()

    def updateSongs(self):
        self.search()
    def changeParameterMinDay(self,k):
        minYear = int(self.comboBoxMinYear.get())
        maxYear = int(self.comboBoxMaxYear.get())
        minMonth = self.comboBoxMinMonth.get()
        maxMonth = self.comboBoxMaxMonth.get()
        if minYear == maxYear and minMonth == maxMonth:
            minDay = int(self.comboBoxMinDay.get())
            maxDay = int(self.comboBoxMaxDay['values'][-1])

            listOfDays = list(range(minDay,maxDay+1))
            self.comboBoxMaxDay['values'] = listOfDays
            self.comboBoxMaxDay.set(listOfDays[0])
        pass

    def changeParameterMaxDay(self,k):
        minYear = int(self.comboBoxMinYear.get())
        maxYear = int(self.comboBoxMaxYear.get())
        minMonth = self.comboBoxMinMonth.get()
        maxMonth = self.comboBoxMaxMonth.get()
        if minYear == maxYear and minMonth == maxMonth:
            #minDay = int(self.comboBoxMinDay.get())
            maxDay = int(self.comboBoxMaxDay.get())

            listOfDays = list(range(1,maxDay+1))
            self.comboBoxMinDay['values'] = listOfDays
            self.comboBoxMinDay.set(listOfDays[0])
        pass

    def changeParameterMinMonth(self,k):
        minYear = int(self.comboBoxMinYear.get())
        maxYear = int(self.comboBoxMaxYear.get())
        minValue = self.comboBoxMinMonth.get()

        listOfDays = list(range(1,self.defaultDaysMax[minValue]+1))
        self.comboBoxMinDay['values'] = listOfDays
        self.comboBoxMinDay.set(listOfDays[0])

        if minYear == maxYear:

            listOfMonths = self.defaultMonths[self.defaultMonths.index(minValue):]
            self.comboBoxMaxMonth['values'] = listOfMonths
            #self.comboBoxMaxMonth.set(listOfMonths[0])
            self.changeParameterMinDay(0)
        pass

    def changeParameterMaxMonth(self,k):
        minYear = int(self.comboBoxMinYear.get())
        maxYear = int(self.comboBoxMaxYear.get())
        maxValue = self.comboBoxMaxMonth.get()
        listOfDays = list(range(1,self.defaultDaysMax[maxValue]+1))
        self.comboBoxMaxDay['values'] = listOfDays
        self.comboBoxMaxDay.set(listOfDays[-1])
        if minYear == maxYear:

            listOfMonths = self.defaultMonths[0:self.defaultMonths.index(maxValue)+1]
            self.comboBoxMinMonth['values'] = listOfMonths
            #self.comboBoxMinMonth.set(listOfMonths[0])
            self.changeParameterMaxDay(0)
        pass

    def changeParameterMinYear(self,k):
        minValue = int(self.comboBoxMinYear.get())
        maxValue = int(self.comboBoxMaxYear['values'][-1])
        listOfYears = list(range(minValue, maxValue+1))
        self.comboBoxMaxYear['values'] = listOfYears
        #self.comboBoxMaxYear.set(listOfYears[-1])
        self.changeParameterMinMonth(0)
        pass
    def changeParameterMaxYear(self,k):
        maxValue = int(self.comboBoxMaxYear.get())
        minValue = int(self.comboBoxMinYear['values'][0])
        listOfYears = list(range(minValue, maxValue+1))
        self.comboBoxMinYear['values'] = listOfYears
        #self.comboBoxMinYear.set(listOfYears[0])
        self.changeParameterMaxMonth(0)
        pass

    def changeParameter(self, k):
        currentParameter = self.comboBoxSearch.get()
        if currentParameter == 'Release Date':
            minMax = self.songObject.getMinMaxDate(self.loginObject.connection)
            print(minMax[0])
            dateMin = datetime.date(1950,2,1)#minMax[0]
            dateMax = datetime.date(2018,2,1)#minMax[1]

            minYear = dateMin.year;
            maxYear = dateMax.year;
            listOfYears = list(range(minYear, maxYear+1))
            self.comboBoxMinYear['values'] = listOfYears
            self.comboBoxMinYear.set(listOfYears[0])
            self.comboBoxMaxYear['values'] = listOfYears
            self.comboBoxMaxYear.set(listOfYears[-1])


            self.labelMin.grid()
            #self.comboBoxMinDay.grid()
            self.comboBoxMinMonth.grid()
            self.comboBoxMinYear.grid()
            self.labelMax.grid()
            #self.comboBoxMaxDay.grid()
            self.comboBoxMaxMonth.grid()
            self.comboBoxMaxYear.grid()

            self.entrySearch.grid_remove()
            pass
        else:
            self.labelMin.grid_remove()
            self.comboBoxMinDay.grid_remove()
            self.comboBoxMinMonth.grid_remove()
            self.comboBoxMinYear.grid_remove()
            self.labelMax.grid_remove()
            self.comboBoxMaxDay.grid_remove()
            self.comboBoxMaxMonth.grid_remove()
            self.comboBoxMaxYear.grid_remove()

            self.entrySearch.grid()

            pass

    def search(self):
        print("pressed search")
        self.tfieldSongs.config(state=NORMAL)
        self.tfieldSongs.delete(1.0,END)
        currentParameter = self.comboBoxSearch.get()

        listOfIDandSongs = []
        self.dictionarySongs = {}
        if currentParameter == 'Song Name':
            searchKey = self.entrySearch.get()
            listOfSongs = self.songObject.searchSongsByName(self.loginObject.connection, searchKey)
            for x in listOfSongs:
                entry = "{}: {}".format(x.getID(), x.name)
                listOfIDandSongs.append(entry)
                self.dictionarySongs[entry] = x.getID()
                self.tfieldSongs.insert("end",x.decodeGeneral())
                self.tfieldSongs.insert("end","\n")
            self.tfieldSongs.config(state=DISABLED)
        elif currentParameter == 'Artist Name':
            searchKey = self.entrySearch.get()
            listOfSongs = self.songObject.searchSongsByArtist(self.loginObject.connection, searchKey)
            for x in listOfSongs:
                entry = "{}: {}".format(x.getID(), x.name)
                listOfIDandSongs.append(entry)
                self.dictionarySongs[entry] = x.getID()
                self.tfieldSongs.insert("end",x.decodeGeneral())
                self.tfieldSongs.insert("end","\n")
            self.tfieldSongs.config(state=DISABLED)
            pass
        elif currentParameter == 'Song Genre':
            searchKey = self.entrySearch.get()
            listOfSongs = self.songObject.searchSongsByGenre(self.loginObject.connection, searchKey)
            for x in listOfSongs:
                entry = "{}: {}".format(x.getID(), x.name)
                listOfIDandSongs.append(entry)
                self.dictionarySongs[entry] = x.getID()
                self.tfieldSongs.insert("end",x.decodeGeneral())
                self.tfieldSongs.insert("end","\n")
            self.tfieldSongs.config(state=DISABLED)
            pass
        elif currentParameter == 'Release Date':
            maxYear = int(self.comboBoxMaxYear.get())
            minYear = int(self.comboBoxMinYear.get())
            maxMonth = (self.comboBoxMaxMonth.get())
            minMonth = (self.comboBoxMinMonth.get())
            maxDate = datetime.date(year = maxYear, month = self.defaultMonths.index(maxMonth)+1, day = self.defaultDaysMax[maxMonth])
            minDate = datetime.date(year = minYear, month = self.defaultMonths.index(minMonth)+1, day = 1)
            listOfSongs = self.songObject.searchSongsByMinMaxDate(self.loginObject.connection, minDate, maxDate)
            for x in listOfSongs:
                entry = "{}: {}".format(x.getID(), x.name)
                listOfIDandSongs.append(entry)
                self.dictionarySongs[entry] = x.getID()
                self.tfieldSongs.insert("end",x.decodeGeneral())
                self.tfieldSongs.insert("end","\n")
            self.tfieldSongs.config(state=DISABLED)

        self.comboBoxOpen['values'] = listOfIDandSongs
        self.comboBoxOpen.set([])


    def open(self):
        print("pressed open")
        entry = self.comboBoxOpen.get()
        if entry =='':
            return
        songID = self.dictionarySongs[entry]
        if id:
            c_openSong(self.root, True, self.loginObject, songID)
#**********************************************************************************************
# END OF Song related windows
#**********************************************************************************************


#**********************************************************************************************
#
# Playlist related windows
#
#**********************************************************************************************
#=======================================
# Window to edit playlist a info
#=======================================
class c_editPlaylistInfo(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject, playlistID):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Edit playlist window")
        self.loginObject = loginObject

        self.playlistID = playlistID

        self.labelDescription = tk.Label(self.root,  text = "Description", font=("Arial", 16))
        self.labelDescription.grid(row = 6, column = 1, columnspan=4, sticky = N+S+E+W)
        self.tfieldDescription= tk.Text(self.root, width = 70, height = 35)
        self.tfieldDescription.grid(row = 7, column = 1, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldDescription.yview)
        self.scrollb.grid(row=2, column=7+3, rowspan = 35,sticky='nsew')
        self.tfieldDescription['yscrollcommand'] = self.scrollb.set

        self.labelName = tk.Label(self.root,  text = "Name", font=("Arial", 16))
        self.labelName.grid(row = 4, column = 1, columnspan=4, sticky = N+S+E+W)
        self.entryName = tk.Entry(self.root, width = 100)
        self.entryName.grid(row = 5, column = 1, columnspan=2, sticky = N+S+E+W)


        self.buttonAccept= ttk.Button(self.root,text="Accept", command=self.accept)
        self.buttonAccept.grid(row = 14, column = 0, rowspan=1, sticky = N+S+E+W)

        self.buttonCancel= ttk.Button(self.root,text="Cancel", command=self.cancel)
        self.buttonCancel.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        self.playlistObject = c_playlist()
        self.playlistInfo = self.playlistObject.getPlaylistDetails(self.loginObject.connection,  self.playlistID)
        name = self.playlistInfo.name
        description = self.playlistInfo.description
        self.tfieldDescription.insert(1.0, description)
        self.entryName.insert(0,name)




    def cancel(self):
        self.close()

    def accept(self):
        newName = self.entryName.get()
        newDescrition = self.tfieldDescription.get("1.0",END)
        self.playlistObject.updateName(self.loginObject.connection, self.playlistID, newName)
        self.playlistObject.updateDescription(self.loginObject.connection, self.playlistID, newDescrition)
        self.close()


#=======================================
# Window to open a playlist and see their info
#=======================================
class c_openPlaylist(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject, playlistID):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Open playlist window")
        self.loginObject = loginObject

        self.playlistID = playlistID


        self.tfieldPlaylist = tk.Text(self.root, width = 30, height = 20)
        self.tfieldPlaylist.grid(row = 1, column = 3, rowspan=35, columnspan = 3,sticky = N+S+E+W)

        self.tfieldSongsHeader = tk.Text(self.root, width = 100, height = 1)
        self.tfieldSongsHeader.grid(row = 1, column = 7, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.tfieldPlaylistsSongs = tk.Text(self.root, width = 100, height = 35)
        self.tfieldPlaylistsSongs.grid(row = 2, column = 7, rowspan=35, columnspan = 3,sticky = N+S+E+W)

        self.songObject = c_song()
        self.tfieldSongsHeader.insert("end",self.songObject.headerGeneral())
        self.tfieldSongsHeader.insert("end","\n")







        self.comboBoxOpen = ttk.Combobox(self.root, values='', state="readonly", height=4)
        self.comboBoxOpen.grid(row = 15, column = 0, columnspan=2, sticky = N+S+E+W)
        self.buttonOpen= ttk.Button(self.root,text="Open song", command=self.open)
        self.buttonOpen.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        self.comboBoxRemove = ttk.Combobox(self.root, values='', state="readonly", height=4)
        self.comboBoxRemove.grid(row = 12, column = 0, columnspan=2, sticky = N+S+E+W)
        self.buttonRemove= ttk.Button(self.root,text="Remove song", command=self.remove)
        self.buttonRemove.grid(row = 13, column = 0, rowspan=1, sticky = N+S+E+W)


        self.buttonEditInfo= ttk.Button(self.root,text="Edit Info", command=self.editInfo)
        self.buttonEditInfo.grid(row = 11, column = 0, rowspan=1, sticky = N+S+E+W)


        self.updateTexts()


    def editInfo(self):
        print("pressed edit info")
        editWindow = c_editPlaylistInfo(self.root, True, self.loginObject, self.playlistID)
        self.root.wait_window(editWindow.root)
        self.updateTexts()

    def updateTexts(self):
        self.tfieldPlaylist.config(state=NORMAL)
        self.tfieldPlaylist.delete(1.0,"end")
        self.playlistObject = c_playlist()
        self.playlistInfo = self.playlistObject.getPlaylistDetails(self.loginObject.connection, self.playlistID)
        playlistText = self.playlistInfo.decodeDetails()
        self.tfieldPlaylist.insert(1.0, playlistText)
        self.songList = self.playlistObject.getSongList(self.loginObject.connection, self.playlistID)
        self.tfieldPlaylist.config(state=DISABLED)

        listOfIDandSongs = []
        self.dictionarySongs = {}
        self.tfieldPlaylistsSongs.config(state=NORMAL)
        self.tfieldPlaylistsSongs.delete(1.0,"end")
        for x in self.songList:
            entry = "{}: {}".format(x.getID(), x.name)
            listOfIDandSongs.append(entry)
            self.dictionarySongs[entry] = x.getID()
            self.tfieldPlaylistsSongs.insert("end",x.decodeGeneral())
            self.tfieldPlaylistsSongs.insert("end","\n")
        self.tfieldPlaylistsSongs.config(state=DISABLED)

        self.comboBoxOpen['values'] = listOfIDandSongs
        self.comboBoxRemove['values'] = listOfIDandSongs

        self.comboBoxOpen.set('')
        self.comboBoxRemove.set('')

    def remove(self):
        print("pressed remove")
        entry = self.comboBoxRemove.get()
        if entry == '':
            return
        songID = self.dictionarySongs[entry]
        if songID:
            playlistObject = c_playlist()
            playlistObject.removeSong(self.loginObject.connection, self.playlistID ,songID)
            self.updateTexts()
    def open(self):
        print("pressed open")
        entry = self.comboBoxOpen.get()
        if entry == '':
            return
        songID = self.dictionarySongs[entry]
        if songID:
            c_openSong(self.root, True, self.loginObject, songID)


#=======================================
# Window to create playlists
#=======================================
class c_createPlaylist(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Create playlist window")
        self.loginObject = loginObject


        self.buttonAccept= ttk.Button(self.root,text="Accept", command=self.accept)
        self.buttonAccept.grid(row = 14, column = 0, rowspan=1, sticky = N+S+E+W)

        self.buttonCancel= ttk.Button(self.root,text="Cancel", command=self.cancel)
        self.buttonCancel.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        self.labelName = tk.Label(self.root,  text = "Name", font=("Arial", 16))
        self.labelName.grid(row = 4, column = 1, columnspan=4, sticky = N+S+E+W)
        self.entryName = tk.Entry(self.root, width = 100)
        self.entryName.grid(row = 5, column = 1, columnspan=4, sticky = N+S+E+W)

        self.labelDescription = tk.Label(self.root,  text = "Description", font=("Arial", 16))
        self.labelDescription.grid(row = 6, column = 1, columnspan=4, sticky = N+S+E+W)
        self.tfieldDescription = tk.Text(self.root, width = 110, height = 35)
        self.tfieldDescription.grid(row = 7, column = 1, rowspan=35, columnspan = 3,sticky = N+S+E+W)

    def cancel(self):
        self.close()

    def accept(self):
        name = self.entryName.get()
        description = self.tfieldDescription.get("1.0",END)
        playlistObject = c_playlist()

        playlistObject.create(self.loginObject.connection,self.loginObject.currentEmail, name, description)
        self.close()

#=======================================
# Window for managing playlists
#=======================================
class c_managePlaylist(c_genericChildWindow):
    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Manage playlist window")
        self.loginObject = loginObject

        self.labelSearch = tk.Label(self.root,  text = "Search by name", font=("Arial", 12))
        self.labelSearch.grid(row = 0, column = 1, columnspan=2, sticky = N+S+E+W)
        self.entrySearch = tk.Entry(self.root, width = 50)
        self.entrySearch.grid(row = 2, column = 1, columnspan=3, sticky = N+S+E+W)
        self.buttonRegister = ttk.Button(self.root,text="Search", command=self.search)
        self.buttonRegister.grid(row = 2, column = 0, rowspan=1, sticky = N+S+E+W)

        #self.entryOpen = tk.Entry(self.root, width = 50)
        #self.entryOpen.grid(row = 16, column = 1, columnspan=4, sticky = N+S+E+W)
        self.comboBoxOpen = ttk.Combobox(self.root, values=[], state="readonly", height=4)
        self.comboBoxOpen.grid(row = 16, column = 1, columnspan=4, sticky = N+S+E+W)
        self.buttonOpen= ttk.Button(self.root,text="Open", command=self.open)
        self.buttonOpen.grid(row = 16, column = 0, rowspan=1, sticky = N+S+E+W)

        ttk.Separator(self.root, orient=HORIZONTAL).grid( row=14, column=0, columnspan=10, sticky='we')


        self.tfieldPlaylists = tk.Text(self.root, width = 90, height = 35)
        self.tfieldPlaylists.grid(row = 1, column = 5, rowspan=35, columnspan = 3,sticky = N+S+E+W)
        self.tfieldPlaylists.config(state=DISABLED)

        self.tfieldAlbumsHeader = tk.Text(self.root, width = 90, height = 1)
        self.tfieldAlbumsHeader.grid(row = 0, column = 5, rowspan=1, columnspan = 3,sticky = N+S+E+W)
        self.playlistObject = c_playlist()
        self.tfieldAlbumsHeader.insert("end",self.playlistObject.headerGeneral())
        self.tfieldAlbumsHeader.insert("end","\n")

        self.scrollb = tk.Scrollbar(self.root, command=self.tfieldPlaylists.yview)
        self.scrollb.grid(row=1, column=6+3, rowspan = 35,sticky='nsew')
        self.tfieldPlaylists['yscrollcommand'] = self.scrollb.set


        self.buttonCreate= ttk.Button(self.root,text="Create", command=self.createPlaylist)
        self.buttonCreate.grid(row = 10, column = 0, rowspan=1, sticky = N+S+E+W)

        self.labelPlaylist = tk.Label(self.root,  text = "To add songs\ngo to search songs", font=("Arial", 16))
        self.labelPlaylist.grid(row = 15, column = 0, rowspan=1, sticky = N+S+E+W)


        self.comboBoxRemove = ttk.Combobox(self.root, values='', state="readonly", height=4)
        self.comboBoxRemove.grid(row = 17, column = 1, columnspan=3, sticky = N+S+E+W)
        self.buttonRemove= ttk.Button(self.root,text="Delete playlist", command=self.removePlaylist)
        self.buttonRemove.grid(row = 17, column = 0, rowspan=1, sticky = N+S+E+W)


        self.updatePlaylists()


    def removePlaylist(self):
        print("pressed remove")
        entry = self.comboBoxRemove.get()
        if entry == '':
            return
        playlistID = self.dictionaryPlaylists[entry]
        if playlistID:
            playlistObject = c_playlist()
            playlistObject.removePlaylist(self.loginObject.connection, playlistID)
            self.updatePlaylists()

    def createPlaylist(self):
        print("pressed createPlaylist")
        editWindow = c_createPlaylist(self.root, True, self.loginObject)
        self.root.wait_window(editWindow.root)
        self.updatePlaylists()


    def search(self):
        print("pressed search")
        self.tfieldPlaylists.config(state=NORMAL)
        self.tfieldPlaylists.delete(1.0,END)
        searchKey = self.entrySearch.get()
        listOfPlaylist = []
        if searchKey == '':
            listOfPlaylist = self.playlistObject.getListOfMyPlaylists(self.loginObject.connection, self.loginObject.currentEmail)
        else:
            listOfPlaylist = self.playlistObject.getListOfMyPlaylistsByName(self.loginObject.connection, self.loginObject.currentEmail, searchKey)
        listOfIDsAndPlaylists = []
        self.dictionaryPlaylists = {}
        for x in listOfPlaylist:
            entry = "{}: {}".format(x.getID(), x.name)
            listOfIDsAndPlaylists.append(entry)
            self.dictionaryPlaylists[entry] = x.getID()
            self.tfieldPlaylists.insert("end",x.decodeGeneral())
            self.tfieldPlaylists.insert("end","\n")
        self.tfieldPlaylists.config(state=DISABLED)
        self.comboBoxOpen.set([])
        self.comboBoxOpen['values'] = listOfIDsAndPlaylists
        self.comboBoxRemove.set([])
        self.comboBoxRemove['values'] = listOfIDsAndPlaylists

    def open(self):
        print("pressed open")
        entry = self.comboBoxOpen.get()
        if entry == '':
            return
        playlistID = self.dictionaryPlaylists[entry]
        if id:
            c_openPlaylist(self.root, True, self.loginObject, playlistID)


    def updatePlaylists(self):
        self.search()
#**********************************************************************************************
# END OF Playlist related windows
#**********************************************************************************************


#=======================================
# Window that allow editors to add or remove other editors
#=======================================
class c_manageEditors(c_genericChildWindow):

    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Manage editors window")

        self.loginObject = loginObject


        self.labelEmail = tk.Label(self.root,  text = "Email of user to manage", font=("Arial", 16))
        self.labelEmail.grid(row = 4, column = 1, columnspan=4, sticky = N+S+E+W)
        self.entryEmail = tk.Entry(self.root, width = 100)
        self.entryEmail.grid(row = 5, column = 1, columnspan=4, sticky = N+S+E+W)

        #self.labelPassword= tk.Label(self.root,  text = "Your password", font=("Arial", 16))
        #self.labelPassword.grid(row = 7, column = 1, rowspan=1, sticky = N+S+E+W)
        #self.entryPassword = tk.Entry(self.root, show="*", width = 100)
        #self.entryPassword.grid(row = 8, column = 1, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Make editor", command=self.makeEditor)
        self.buttonRegister.grid(row = 7, column = 1, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Remove from editor", command=self.removeEditor)
        self.buttonRegister.grid(row = 7, column = 2, rowspan=2, sticky = N+S+E+W)


        self.labelError = tk.Label(self.root,  text = "Error", font=("Arial", 16))
        self.labelError.grid(row = 9, column = 1, rowspan=2, columnspan = 10, sticky = N+S+E+W)
        self.labelError.grid_remove()

    def makeEditor(self):
        print("pressed makeEditor")
        userToChange = self.entryEmail.get()
        check = self.loginObject.makeAdmin(self.loginObject.connection, userToChange)
        if check:
            self.labelError.grid()
            self.labelError['text'] = "User \"{}\" did not exist".format(userToChange)
        else:
            self.labelError.grid()
            self.labelError['text'] = "Success user \"{}\" is now an editor".format(userToChange)

    def removeEditor(self):
        print("pressed removeEditor")
        userToChange = self.entryEmail.get()
        check = self.loginObject.removeAdmin(self.loginObject.connection, userToChange)
        if check:
            self.labelError.grid()
            self.labelError['text'] = "User \"{}\" did not exist".format(userToChange)
        else:
            self.labelError.grid()
            self.labelError['text'] = "Success user \"{}\" is not an editor anymore".format(userToChange)

#=======================================
# Window with the first menu after login, with all the functions
#=======================================
class c_firstMenu(c_genericChildWindow):

    def __init__(self, topLevel, hideTopLevel, loginObject):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("First menu window")

        self.loginObject = loginObject
        self.admin = loginObject.sessionIsAdmin

        self.labelName = tk.Label(self.root,  text = "Name", font=("Arial", 16))
        self.labelName.grid(row = 1, column = 0, rowspan=1, sticky = N+S+E+W)
        self.labelName['text'] = self.loginObject.currentEmail

        self.buttonRegister = ttk.Button(self.root,text="logout", command=self.logOut)
        self.buttonRegister.grid(row = 8, column = 0, rowspan=2, sticky = N+S+E+W)

        #ttk.Separator(self.root, orient=VERTICAL).grid(column=1, row=0, rowspan=500, sticky='ns')
        canvas = Canvas(self.root, width=50, height=500)
        canvas.grid(column=1, row=0, rowspan=38, sticky='ns')
        canvas.configure(background = 'black')


        self.buttonRegister = ttk.Button(self.root,text="Search album list", command=self.searchAlbum)
        self.buttonRegister.grid(row = 2, column = 3, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Search music list", command=self.searchMusic)
        self.buttonRegister.grid(row = 8, column = 3, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Search artist", command=self.searchArtist)
        self.buttonRegister.grid(row = 14, column = 3, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Manage my playlists", command=self.managePlaylist)
        self.buttonRegister.grid(row = 20, column = 3, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Manage my files", command=self.manageFiles)
        self.buttonRegister.grid(row = 26, column = 3, rowspan=2, sticky = N+S+E+W)


        if self.admin:
            self.buttonRegister = ttk.Button(self.root,text="Manage editors", command=self.manageEditors)
            self.buttonRegister.grid(row = 32, column = 3, rowspan=2, sticky = N+S+E+W)


    def searchAlbum(self):
        print("pressed searchAlbum")
        c_searchAlbum(self.root, True, self.loginObject)
        pass

    def searchMusic(self):
        print("pressed searchMusic")
        c_searchMusic(self.root, True, self.loginObject)
        pass
    def managePlaylist(self):
        print("pressed managePlaylist")
        c_managePlaylist(self.root, True, self.loginObject)
        pass
    def manageFiles(self):
        print("pressed manageFiles")
        pass
    def searchArtist(self):
        print("pressed searchArtist")

    def manageEditors(self):
        print("pressed manageEditors")
        c_manageEditors(self.root, True, self.loginObject)



    def logOut(self):
        print("pressed logout")
        self.close()
        pass


#=======================================
# Window to register a user
#=======================================
class c_registerWindow(c_genericChildWindow):

    def __init__(self, topLevel, hideTopLevel):
        super().__init__(topLevel, hideTopLevel)
        self.root.title("Register menu window")

        self.labelEmail = tk.Label(self.root,  text = "Email", font=("Arial", 16))
        self.labelEmail.grid(row = 4, column = 1, columnspan=4, sticky = N+S+E+W)
        self.entryEmail = tk.Entry(self.root, width = 100)
        self.entryEmail.grid(row = 5, column = 1, columnspan=4, sticky = N+S+E+W)



        self.labelPassword= tk.Label(self.root,  text = "Password", font=("Arial", 16))
        self.labelPassword.grid(row = 7, column = 1, rowspan=1, sticky = N+S+E+W)
        self.entryPassword = tk.Entry(self.root, show="*", width = 100)
        self.entryPassword.grid(row = 8, column = 1, rowspan=2, sticky = N+S+E+W)


        self.labelName = tk.Label(self.root,  text = "Name", font=("Arial", 16))
        self.labelName.grid(row = 10, column = 1, rowspan=1, sticky = N+S+E+W)
        self.entryName= tk.Entry(self.root, width = 100)
        self.entryName.grid(row = 11, column = 1, rowspan=2, sticky = N+S+E+W)



        self.buttonRegister = ttk.Button(self.root,text="Register", command=self.register)
        self.buttonRegister.grid(row = 13, column = 1, rowspan=2, sticky = N+S+E+W)

        self.labelError = tk.Label(self.root,  text = "Error", font=("Arial", 16))
        self.labelError.grid(row = 16, column = 1, rowspan=2, sticky = N+S+E+W)
        self.labelError.grid_remove()




    def register(self):

        email = self.entryEmail.get()
        password = self.entryPassword.get()
        name = self.entryName.get()

        if email == '' or password == '' or name == '':
            self.labelError.grid()
            self.labelError['text'] = "Error: can't have any field empty"

        else:


            connection = psycopg2.connect(host="localhost",database="DropMusic", user="postgres", password="postgres")
            loginObject = c_login()
            check = loginObject.register(connection, email, password, name)
            if check == 'Already exists':
                self.labelError.grid()
                self.labelError['text'] = "Error: User already exists"
            else:
                self.labelError.grid()
                self.labelError['text'] = "User registered"


        self.entryPassword.delete(0,END)

#=======================================
# First window of the program. Allows login and to open register window
#=======================================
class c_startWindow():

    def __init__(self, root):
        self.root = root
        self.root.resizable(False, False)

        self.connection = None
        self.loginObject = c_login()
        self.root.title("Login menu window")
        #start menu
        self.labelEmail = tk.Label(self.root,  text = "Email", font=("Arial", 16))
        self.labelEmail.grid(row = 4, column = 1, columnspan=4, sticky = N+S+E+W)
        self.entryEmail = tk.Entry(self.root, width = 100)
        self.entryEmail.grid(row = 5, column = 1, columnspan=4, sticky = N+S+E+W)



        self.labelPassword= tk.Label(self.root,  text = "Password", font=("Arial", 16))
        self.labelPassword.grid(row = 7, column = 1, rowspan=1, sticky = N+S+E+W)
        self.entryPassword = tk.Entry(self.root, show="*", width = 100)
        self.entryPassword.grid(row = 8, column = 1, rowspan=2, sticky = N+S+E+W)




        self.buttonLogin = ttk.Button(self.root,text="Login", command=self.login)
        self.buttonLogin.grid(row = 10, column = 1, rowspan=2, sticky = N+S+E+W)

        self.buttonRegister = ttk.Button(self.root,text="Register", command=self.register)
        self.buttonRegister.grid(row = 12, column = 1, rowspan=2, sticky = N+S+E+W)

        self.labelError = tk.Label(self.root,  text = "Error", font=("Arial", 16))
        self.labelError.grid(row = 16, column = 1, rowspan=2, sticky = N+S+E+W)
        self.labelError.grid_remove()

        self.registerWindow = None


    def login(self):
        print("pressed login button")
        email = self.entryEmail.get()
        password = self.entryPassword.get()

        self.connection = psycopg2.connect(host="localhost",database="DropMusic", user="postgres", password="postgres")
        type = self.loginObject.login(self.connection, email, password)
        if type:
            if type == "admin":
                self.labelError.grid()
                self.labelError['text'] = "User is an admin"
            elif type == 'user':
                self.labelError.grid()
                self.labelError['text'] = "User is not an admin"
            firstMenu = c_firstMenu(self.root, True, self.loginObject)
            self.entryPassword.delete(0,END)
            self.root.wait_window(firstMenu.root)
            if self.connection is not None:
                self.connection.close()

        else:
            self.labelError.grid()
            self.labelError['text'] = "login fail"

        self.entryPassword.delete(0,END)
        pass

    def register(self):
        print("pressed register button")
        self.registerWindow = c_registerWindow(self.root, True)



        pass



if __name__ == '__main__':
    root = tk.Tk()

    console = c_startWindow(root)

    root.mainloop()
