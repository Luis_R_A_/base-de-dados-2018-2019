
--------------------------------------------------------------------------
--        ALBUMS
--------------------------------------------------------------------------

-- Show Albums List. 
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM            albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- Show Album List from artist ID.
-- (1)
        -- replace 3096 with artist id
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE   ar.id = 3096
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- Search Album List by NAME.
-- (1)
        -- replace '%%' with album partial name ('%'+name+'%') (String)
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE           UPPER(al.name)          LIKE UPPER('%%')
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- Search Album List by publisher.
-- (1)
        -- replace '%%' with publisher partial name ('%'+name+'%') (String)
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
JOIN            publishers p            ON al.publishers_id = p.id
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE           UPPER(p.name)           LIKE UPPER('%%')
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- Search Album List by genre.
-- (1)
        -- replace '%%' with genre partial name ('%'+name+'%') (String)
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist", 
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN       genres_albums g_al      ON g_al.albums_id = al.id
LEFT JOIN       genres g                ON g_al.genres_name = g.name
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE           UPPER(g.name)           LIKE UPPER('%%')
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- Search Album List by genre ID. 
-- (1)
        -- replace 'Alternative' with genre name (String)
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN       genres_albums g_al      ON g_al.albums_id = al.id
LEFT JOIN       genres g                ON g_al.genres_name = g.name
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE           g.name = 'Alternative'
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- create new album
-- (1)
        -- replace 'New Name' with name (String)
        -- replace '2015-06-24' with release date (yyyy-mm-dd) (String)
        -- replace 2 with publisher id 
INSERT  INTO albums (name, albumcreationdate, publishers_id)
        VALUES ('Album Name', DATE '2015-06-24', 2)
;

-- delete album
-- (1)
        -- replace 1 with album id 
DELETE
FROM    albums
WHERE   id = 1;
;

-- edit album info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'New Name' with name (String)
        -- replace 'yyyy-mm-dd' with new date (String)
        -- replace 'New Description' with new description (String)
        -- replace 'photo.png' with new photo path (String)
        -- replace 2 with publisher id 
        -- replace 1 with album id 
UPDATE  albums
SET     name = 'New name'
--SET     albumcreationdate = DATE 'yyyy-mm-dd'
--SET     description = 'New Description'
--SET     photofilename = 'photo.png'
--SET     publishers_id = 2
WHERE   id = 1
;

-- add albums genres
-- (1)
        -- replace 'Alternative' with genre name (String)
        -- replace 1 with album id
INSERT  INTO genres_albums (genres_name, albums_id)
        VALUES ('Alternative', 1)
;

-- remove albums genres
-- (1)
        -- replace 'Alternative' with genre name (String)
        -- replace 1 with album id
DELETE
FROM    genres_albums
WHERE   genres_name = 'Alternative' AND
        albums_id = 1
;

-- add albums artist
-- (1)
        -- replace 3103 with artist id
        -- replace 1 with album id
INSERT  INTO artist_albums (artist_id, albums_id)
        VALUES (3103, 1)
;

-- remove albums artist
-- (1)
        -- replace 3103 with artist id
        -- replace 1 with album id
DELETE
FROM    artist_albums
WHERE   artist_id = 3103 AND
        albums_id = 1
;

-- Show Album info by ID (Dookie)
-- (1)
        -- replace 1 with album id
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct g.name),'/') AS "Genre",
        al.description                                  AS "Description",
        p.name                                          AS "Publisher",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        al.albumcreationdate                            AS "Release Date", 
        AVG(c.rating)                                   AS "Rating"
FROM    albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN       genres_albums g_al      ON g_al.albums_id = al.id
LEFT JOIN       genres g                ON g_al.genres_name = g.name
JOIN            publishers p            ON al.publishers_id = p.id
JOIN            artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN            artist ar               ON ar_al.artist_id = ar.id
WHERE           al.id = 1
GROUP BY al.id,
        al.photo,
        al.name,
        al.description,
        p.name,
        al.albumcreationdate
ORDER BY al.name
;

--------------------------------------------------------------------------
--        SONGS
--------------------------------------------------------------------------

-- Show Song List (And search by name)
-- (1)
        -- replace '%%' with song partial name ('%'+name+'%') (String)
SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON s.albums_id = al.id
WHERE           UPPER(s.name)           LIKE UPPER('%%')
GROUP BY        s.id,
                al.name
;

-- Show Album song List from Album ID (Dookie)
-- (1)
        -- replace 1 with album id
SELECT  s.id,
        s.name          AS "Name",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
WHERE           s.albums_id = 1
GROUP BY        s.id
;

-- Show Playlist song List from Playlist ID 
-- (1)
        -- replace 2 with playlist id
SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON al.id = s.albums_id
JOIN            playlists_songs pl_s    ON pl_s.songs_id = s.id
JOIN            playlists pl            ON pl_s.playlists_id = pl.id
WHERE           pl.id = 2
GROUP BY        s.id,
                al.name
;

-- Search Song List by artist name
-- (1)
        -- replace '%%' with artist partial name ('%'+name+'%') (String)
SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       artist_songs ar_s       ON ar_s.songs_id = s.id
LEFT JOIN       artist ar               ON ar_s.artist_id = ar.id
WHERE           UPPER(ar.name)          LIKE UPPER('%%')
GROUP BY        s.id,
                al.name
;

-- Search Song List by genre name
-- (1)
        -- replace '%%' with genre partial name ('%'+name+'%') (String)
SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       genres_songs g_s   ON g_s.songs_id = s.id
LEFT JOIN       genres g           ON g_s.genres_name = g.name
WHERE           UPPER(g.name)           LIKE UPPER('%%')
GROUP BY s.id,
        al.name
ORDER BY s.id,
        al.name
;

-- Returns first and last release date on DB (album or song)
SELECT  CASE    WHEN min(s.songreleasedate) IS NOT NULL 
                        THEN min(s.songreleasedate)
                WHEN min(al.albumcreationdate) IS NOT NULL
                        THEN min(al.albumcreationdate)
                ELSE DATE '1500-1-1'
        END,
        CASE    WHEN max(s.songreleasedate) IS NOT NULL 
                        THEN max(s.songreleasedate)
                WHEN max(al.albumcreationdate) IS NOT NULL
                        THEN max(al.albumcreationdate)
                ELSE CURRENT_DATE
        END
FROM    songs s, albums al
;

-- Search song list by release date
-- If release date is null, album release date is used
-- Needs two dates (max, min) 
-- (1)
        -- replace '1990-1-1' with min DATE ('yyyy-mm-dd') (String)
        -- replace '2015-1-1' with max DATE ('yyyy-mm-dd') (String)
SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON s.albums_id = al.id
WHERE           (CASE    WHEN s.songreleasedate IS NOT NULL
                                THEN s.songreleasedate 
                        WHEN al.albumcreationdate IS NOT NULL
                                THEN al.albumcreationdate
                        ELSE NULL
                END BETWEEN DATE '1990-1-1' AND DATE '2015-1-1') OR
                s.songreleasedate IS NULL OR
                al.albumcreationdate IS NULL
GROUP BY s.id,
        al.name
;

-- Search Song List by name, artist, genre and release date
-- (Owned and public access files only)
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace '%%' with song partial name ('%'+name+'%') (String)
        -- replace '%%' with artist partial name ('%'+name+'%') (String)
        -- replace '%%' with genre partial name ('%'+name+'%') (String)
        -- replace '1990-1-1' with min DATE ('yyyy-mm-dd') (String)
        -- replace '2015-1-1' with max DATE ('yyyy-mm-dd') (String)
SELECT  s.id,
        s.name                  AS "Name",
        al.name                 AS "Album",
        s.duration              AS "Duration",
        f.songfilename          AS "File",
        AVG(c.rating)           AS "Rating"
FROM            songs s
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       files f                 ON f.songs_id = s.id
JOIN            users u                 ON (f.users_emaillogin = u.emaillogin OR
                                           f.private = FALSE)
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
LEFT JOIN       genres_songs g_s        ON g_s.songs_id = s.id
LEFT JOIN       genres g                ON g_s.genres_name = g.name
LEFT JOIN       artist_songs ar_s       ON ar_s.songs_id = s.id
LEFT JOIN       artist ar               ON ar_s.artist_id = ar.id
WHERE   u.emaillogin = 'email@mail.com' AND
        UPPER(s.name)           LIKE UPPER('%%') AND
        UPPER(ar.name)          LIKE UPPER('%%') AND
        UPPER(g.name)           LIKE UPPER('%%') AND
        CASE WHEN s.songreleasedate IS NOT NULL
                THEN s.songreleasedate 
             WHEN al.albumcreationdate IS NOT NULL
                THEN al.albumcreationdate
             ELSE now()
        END BETWEEN DATE '1990-1-1' AND DATE '2015-1-1'
GROUP BY s.id,
        s.name,
        al.name,
        s.duration,
        f.songfilename
;

-- create new song
-- (1)
        -- replace 'Song Name' with song name (String)
        -- replace 1 with album id
        -- replace '2014-05-23' with song release date (String)
        -- replace '02:37' with song time (String)
INSERT  INTO songs (name, albums_id, songreleasedate, duration)
        VALUES ('Song Name', 1, DATE '2014-05-23', TIME '02:37')
;

-- delete song by ID
-- (1)
        -- replace 1 with song id
DELETE
FROM    songs
WHERE   id = 1
;

-- edit song info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'New Name' with name (String)
        -- replace 'yyyy-mm-dd' with new date (String)
        -- replace 'mm:ss' with new time (String)
        -- replace 2 with album id 
        -- replace 1 with song id 
UPDATE  songs
SET     name = 'New Name'
--SET     songreleasedate = DATE 'yyyy-mm-dd'
--SET     duration = TIME 'mm:ss'
--SET     albums_id = 2
WHERE   id = 1
;

-- add song artist
-- (1)
        -- replace 3103 with artist id 
        -- replace 1 with song id 
INSERT  INTO artist_songs (artist_id, songs_id)
        VALUES (3103, 1)
;

-- remove song artist
-- (1)
        -- replace 3103 with artist id 
        -- replace 1 with song id 
DELETE
FROM    artist_songs
WHERE   artist_id = 3103 AND
        songs_id = 1
;

-- add song genres
-- (1)
        -- replace 'Alternative' with genre id 
        -- replace 1 with song id 
INSERT  INTO genres_songs (genres_name, songs_id)
        VALUES ('Alternative', 1)
;

-- remove song genres
-- (1)
        -- replace 'Alternative' with genre id 
        -- replace 1 with song id 
DELETE
FROM    genres_songs
WHERE   genres_name = 'Alternative' AND
        songs_id = 1
;

-- Show song info by ID
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace 1 with song id
SELECT  s.id,
        s.name                                          AS "Name",
        al.name                                         AS "Album",
        s.duration                                      AS "Duration",
        AVG(c.rating)                                   AS "Rating",
        s.songreleasedate                               AS "Release Date",
        array_to_string(array_agg(distinct g.name),'/') AS "Genre",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artists",
        CASE    WHEN (l.lyricstext) IS NOT NULL THEN l.lyricstext
                ELSE ''
        END                                             AS "Lyric",
        CASE    WHEN    (f.private = FALSE AND 
                        f.users_emaillogin = 'email@mail.com')
                        THEN f.songfilename
                ELSE 'No file'
        END                                             AS "Files"
FROM    songs s
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       files f                 ON s.id = f.songs_id
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
LEFT JOIN       genres_songs g_s        ON g_s.songs_id = s.id
LEFT JOIN       genres g                ON g_s.genres_name = g.name
LEFT JOIN       artist_songs ar_s       ON ar_s.songs_id = s.id
LEFT JOIN       artist ar               ON ar_s.artist_id = ar.id
LEFT JOIN       lyrics l                ON l.songs_id = s.id
WHERE           s.id = 1
GROUP BY s.id,
        s.name,
        al.name,
        s.duration,
        l.lyricstext,
        f.private,
        f.users_emaillogin
;

--------------------------------------------------------------------------
--        ARTISTS
--------------------------------------------------------------------------

-- Show Artist list and filter by name
-- (1)
        -- replace '%%' with artist partial name ('%'+name+'%') (String)
SELECT  a.id,
        a.name,
        a.tipo
FROM artist a
WHERE UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- Show Band list
-- (1)
        -- replace '%%' with band partial name ('%'+name+'%') (String)
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'band' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- Show Band by id
-- (1)
        -- replace 1 with artist id
SELECT  a.id,
        a.photo,
        a.name,
        b.groupcreationdate,
        b.story
FROM    artist a
JOIN    band b  ON b.artist_id = a.id
WHERE   a.id = 1
;

-- Show Musician list
-- (1)
        -- replace '%%' with musician partial name ('%'+name+'%') (String)
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'musician' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- Show Musician by id
-- (1)
        -- replace 1 with artist id
SELECT  a.id,
        a.photo,
        a.name,
        m.birthdate,
        m.nationality
FROM    artist a
JOIN    musician m  ON m.artist_id = a.id
WHERE   a.id = 1
;

-- Show Composer list
-- (1)
        -- replace '%%' with composer partial name ('%'+name+'%') (String)
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'composer' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- Show Composer by id
-- (1)
        -- replace 1 with artist id
SELECT  a.id,
        a.photo,
        a.name,
        c.birthdate,
        c.nationality
FROM    artist a
JOIN    composer c  ON c.artist_id = a.id
WHERE   a.id = 1
;

-- Show artist info by ID
-- (1)
        -- replace 3103 with artist id
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.id = 3103
;

-- create new artist
-- (1)
        -- replace 'New Artist' with new artist name (String)
        -- replace 'artist' with artist type (band, musician or composer) (String)
INSERT  INTO artist (name, tipo)
        VALUES ('New Artist','artist')
;

-- edit artist info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'photo.png' with new photo file path (String)
        -- replace 'New Name' with new name (String)
        -- replace 1 with artist id
UPDATE  artist
SET     photofilename = 'photo.png'
--SET     name = 'New Name'
WHERE   id = 1
;

-- edit musician info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace '2014-05-3' with new Date ('yyyy-mm-dd') (String)
        -- replace 'Mexican' with new nationality (String)
        -- replace 1 with artist id 
UPDATE  musician
SET     birthdate = DATE '2014-05-3'
--SET     nationality = 'Mexican'
WHERE   artist_id = 1
;

-- edit composer info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace '2014-05-3' with new Date ('yyyy-mm-dd') (String)
        -- replace 'Mexican' with new nationality (String)
        -- replace 1 with artist id 
UPDATE  composer
SET     birthdate = DATE '2014-05-3'
--SET     nationality = 'Mexican'
WHERE   artist_id = 1
;

-- edit band info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace '2014-05-3' with new Date ('yyyy-mm-dd') (String)
        -- replace 'Created when they nothing better to do' with new story (String)
        -- replace 1 with artist id 
UPDATE  band
SET     groupcreationdate = DATE '2014-05-3'
--SET     story = 'Created when they nothing better to do'
WHERE   artist_id = 1
;

-- edit band info (add member)
-- (1)
        -- replace 3 with band id
        -- replace 3103 with musician id
INSERT  INTO band_musician (band_artist_id, musician_artist_id)
        VALUES (3, 3103)
;

-- edit band info (remove member)
-- (1)
        -- replace 3 with band id
        -- replace 3103 with musician id
DELETE
FROM    band_musician
WHERE   band_artist_id = 3 AND
        musician_artist_id = 3103
;

--------------------------------------------------------------------------
--        PLAYLISTS
--------------------------------------------------------------------------

-- Create new playlist
-- (1)
        -- replace 'Name Surname''s Liked Songs' with user (name + '''s Liked Songs') (String)
INSERT  INTO playlists (name, description, playlistcreationdate, private) 
        VALUES ('Like', 'Name Surname''s Liked Songs', CURRENT_DATE, TRUE);


-- Show public playlists and playlists from user ID
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace '%%' with playlist partial name ('%'+name+'%') (String)
SELECT  pl.id,
        pl.name         AS "Name",
        u.emaillogin    AS "Owner"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   (pl.private = FALSE     OR u.emaillogin = 'email@mail.com') AND
        LOWER(pl.name) LIKE LOWER('%%')
;

-- Show  playlists from user ID
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace '%%' with playlist partial name ('%'+name+'%') (String)
SELECT  pl.id,
        pl.name         AS "Name",
        u.emaillogin    AS "Owner"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   (u.emaillogin = 'email@mail.com') AND
        LOWER(pl.name) LIKE LOWER('%%')
;


-- Share playlist with user
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace 4 with playlist id
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('email@mail.com', 4);

-- remove shared playlist
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace 4 with playlist id
DELETE
FROM    users_playlists
WHERE   users_emaillogin = 'email@mail.com' AND --no longer friends
        playlists_id = 4
;

-- edit playlist info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'New Name' with name (String)
        -- replace 'Really liked songs?' with new Description (every (')char must become ('')) (String)
        -- replace 'yyyy-mm-dd' with new date (String)
        -- replace FALSE with new authorization 
        -- replace 1 with playlist id
UPDATE  playlists
SET     name = 'New Name'
--SET     description = 'Really liked songs?'
--SET     playlistcreationdate = DATE 'yyyy-mm-dd'
--SET     private = FALSE
WHERE   id = 1
;

-- edit playlist songs (add)
-- (1)
        -- replace 1 with playlist id
        -- replace 3 with song id
INSERT  INTO playlists_songs (playlists_id, songs_id)
        VALUES (1, 3)
;

-- edit playlist songs (remove)
-- (1)
        -- replace 1 with playlist id
        -- replace 3 with song id
DELETE
FROM    playlists_songs
WHERE   playlists_id = 1 AND
        songs_id = 3
;

-- Show playlist info by ID
-- (1)
        -- replace 4 with playlist id
SELECT  pl.id,
        pl.name                 AS "Name",
        pl.description          AS "Description",
        pl.playlistcreationdate AS "Creation Date",
        pl.private,
        u.emaillogin            AS "Owner"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   pl.id = 4
LIMIT   1
;

--------------------------------------------------------------------------
--        USERS
--------------------------------------------------------------------------

-- User login 
-- returns emaillogin on success
-- returns null on fail
-- (1)
        -- replace 'email@mail.com' with user id
        -- replace 'easy' with user password
SELECT  u.emaillogin
FROM    users u
WHERE   LOWER(u.emaillogin) = LOWER('email@mail.com') AND
        u.password = crypt('easy', u.password)
;

-- search users by name
-- (1)
        -- replace both '%%' with user partial name ('%'+name+'%') (String)
SELECT  emaillogin,
        name
FROM    users
WHERE   LOWER(emaillogin) LIKE LOWER('%%') OR
        LOWER(name) LIKE LOWER('%%')
;

-- Create new user (Register)
-- (1)
        -- replace 'email@mail.com' with user email (String)
        -- replace 'Name Surname' with user name (String)
        -- replace 'Name Surname' with user name (String)
        -- replace 'password' with user password (String)
INSERT  INTO users (emaillogin, name, password, admin) 
        VALUES ('email@mail.com', 'Name Surname', crypt('password', gen_salt('bf', 8)), FALSE);

-- edit user privileges
-- (1)
        -- replace TRUE with new authorization
        -- replace 'email@mail.com' with user id 
UPDATE  users
SET     admin = TRUE
WHERE   emaillogin = 'email@mail.com'
;

-- edit user info
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'New Name' with name (String)
        -- replace 'New Password' with password (String)
        -- replace 'email@mail.com' with user id 
UPDATE  users
SET     name = 'New Name'
--SET     password = crypt('New Password', gen_salt('bf', 8))
WHERE   emaillogin = 'email@mail.com'
;

--------------------------------------------------------------------------
--        LYRICS
--------------------------------------------------------------------------

-- Show Lyric by song id
-- (1)
        -- replace 1 with song id
SELECT  l.lyricstext
FROM    lyrics l
WHERE   l.songs_id = 1
;

-- Insert lyrics (Burnout)
-- (1)
        -- replace 'Very short music x20' with lyrics text (every (')char must become ('')) (String)
        -- replace 1 with song id
INSERT INTO lyrics (lyricstext, songs_id)
VALUES      ('Very short music x20' ,1)
;

-- Remove lyrics (Burnout)
-- (1)
        -- replace 1 with song id
DELETE
FROM    lyrics
WHERE   songs_id = 1
;

--------------------------------------------------------------------------
--        COMMENTS
--------------------------------------------------------------------------
-- Open Album Comments
-- (1)
        -- replace 1 with album id
SELECT  c.id,
        u.emaillogin            AS "User",
        c.comment               AS "Comment",
        c.commentcreationdate   AS "Comment Date",
        c.rating                AS "Rating",
        c.vote                  AS "Votes"
FROM    comments c
JOIN    users u                 ON u.emaillogin = c.users_emaillogin
JOIN    album_comments al_c     ON al_c.comments_id = c.id
JOIN    albums al               ON al.id = al_c.albums_id
WHERE   al.id = 1
;

-- Open Song Comments
-- (1)
        -- replace 1 with song id
SELECT  c.id,
        u.emaillogin            AS "User",
        c.comment               AS "Comment",
        c.commentcreationdate   AS "Comment Date",
        c.rating                AS "Rating",
        c.vote                  AS "Votes"
FROM    comments c
JOIN    users u                 ON u.emaillogin = c.users_emaillogin
JOIN    songs_comments s_c      ON s_c.comments_id = c.id
JOIN    songs s                 ON s.id = s_c.songs_id
WHERE   s.id = 1
;

-- Create New Comment
-- (1)
        -- replace 'New Comment' with comment (String)
        -- replace 5 with rating 
        -- replace 'email@mail.com' with user id (String)
INSERT  INTO comments (comment, rating, commentcreationdate, vote, users_emaillogin)
        VALUES ('New Comment', 5, CURRENT_DATE, 0, 'email@mail.com')
;

-- Add comment to song
-- (1)
        -- replace 1 with comment id
        -- replace 16 with song id 
INSERT  INTO songs_comments (comments_id, songs_id)
        VALUES (1, 16)
;

-- Add comment to album
-- (1)
        -- replace 1 with comment id
        -- replace 2 with album id 
INSERT  INTO album_comments (comments_id, albums_id)
        VALUES (1, 2)
;

-- Delete comment
-- (1)
        -- replace 1 with comment id
DELETE
FROM    comments
WHERE   id = 1
;

-- Vote in comment
-- (1)
        -- uncomment vote to make (+ or -)
-- (2)
        -- replace 2 with comment id
UPDATE  comments
SET     vote = vote + 1
--SET     vote = CASE     WHEN vote = 0 THEN 0
--                        ELSE vote - 1
--               END
WHERE   id = 2
;

-- Edit comment
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'New Comment' with new comment
        -- replace 5 with new rating
        -- replace 1 with comment id
UPDATE  comments
SET     comment = 'New Comment'
--SET     rating = 5
WHERE   id = 1
;

--------------------------------------------------------------------------
--        CONCERTS
--------------------------------------------------------------------------
-- Show Concert List
SELECT  c.id,
        c.name          AS "Concert",
        c.location      AS "Local",
        c.concertdate   AS "Date"
FROM    concerts c
;

-- Search Concert by name, location, date or artist
-- (1)
        -- replace '%%' with ('%' + artist name + '%') (String)
        -- replace '%%' with ('%' + concert name + '%') (String)
        -- replace '%%' with ('%' + location + '%') (String)
        -- replace '1990-1-1' with min date (String)
        -- replace '2030-1-1' with max date (String)
SELECT  c.id,
        c.name          AS "Concert",
        c.location      AS "Local",
        c.concertdate   AS "Date"
FROM    concerts c
LEFT JOIN concerts_artist c_a   ON c_a.concerts_id = c.id
JOIN    artist a                ON a.id = c_a.artist_id
WHERE   LOWER(a.name)           LIKE LOWER('%%') AND
        LOWER(c.name)           LIKE LOWER('%%') AND
        LOWER(c.location)       LIKE LOWER('%%') AND
        c.concertdate BETWEEN DATE '1990-1-1' AND DATE '2030-1-1'
;


-- Create
-- (1)
        -- replace 'Concert Name' with name (String)
        -- replace 'City' with location (String)
        -- replace '2019-05-02' with concert (String)
INSERT  INTO concerts (name, location, concertdate)
        VALUES ('Concert Name', 'City', DATE '2019-05-02')
;

-- Delete
-- (1)
        -- replace 1 with concert id
DELETE
FROM    concerts
WHERE   id = 1
;

-- Add artist
-- (1)
        -- replace 1 with concert id
        -- replace 2 with artist id
INSERT  INTO concerts_artist (concerts_id, artist_id)
        VALUES (1, 2)
;

-- Remove artist
-- (1)
        -- replace 1 with concert id
        -- replace 2 with artist id
DELETE
FROM    concerts_artist
WHERE   concerts_id = 1 AND
        artist_id = 2
;

--------------------------------------------------------------------------
--        FILES
--------------------------------------------------------------------------

-- Create file
-- (1)
        -- replace 'email@mail.com' with owner user id (String)
        -- replace 'file path' with song file path (String)
INSERT  INTO files (users_emaillogin, songfilename, private)
        VALUES ('email@mail.com', 'file path', TRUE)
;

-- Delete file
-- (1)
        -- replace 'email@mail.com' with owner user id
        -- replace 1 with song id
DELETE
FROM    files f
WHERE   users_emaillogin = 'email@mail.com' AND
        songs_id = 1
;

-- Delete file
-- (1)
        -- uncomment line for required change
-- (2)
        -- replace 'email@mail.com' with owner user id
        -- replace 1 with song id
UPDATE  files
SET     private = FALSE
--SET     private = TRUE
WHERE   users_emaillogin = 'email@mail.com' AND
        songs_id = 1
;

--------------------------------------------------------------------------
--        EDITORS
--------------------------------------------------------------------------

-- edit publisher info
-- (1)
        -- replace 'New Name' with name (String)
        -- replace 1 with publisher id 
UPDATE  publishers
SET     name = 'New Name'
WHERE   id = 1
;

-- edit genres (add)
-- (1)
        -- replace 'NEW GENRE' with new genre name (String)
INSERT  INTO genres (name)
        VALUES ('NEW GENRE')
;

-- edit genres (remove)
-- (1)
        -- replace 'NEW GENRE' with new genre name (String)
DELETE
FROM    genres
WHERE   name = 'NEW GENRE'
;
