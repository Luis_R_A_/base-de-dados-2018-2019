INSERT INTO artist (name, tipo) VALUES ('Johann Sebastian Bach', 'composer');
INSERT INTO artist (name, tipo) VALUES ('Wolfgang Amadeus Mozart', 'composer');
INSERT INTO artist (name, tipo) VALUES ('Ludwig van Beethoven', 'composer');
INSERT INTO artist (name, tipo) VALUES ('Billie Joe Armstrong', 'composer');

--insert composers in composer table--
INSERT INTO composer (artist_id)
SELECT artist.id
FROM artist
WHERE artist.tipo = 'composer';

UPDATE composer
SET birthdate = DATE '1685-03-31'
WHERE artist_id = ( SELECT  id
                    FROM    artist
                    WHERE   name = 'Johann Sebastian Bach')
;
