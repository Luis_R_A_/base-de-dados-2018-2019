-- Insert users
INSERT INTO users (emaillogin, name, password, admin) VALUES ('eduardo.ff.rodrigues@gmail.com', 'Eduardo Rodrigues', crypt('vai-esta', gen_salt('bf', 8)), TRUE);
INSERT INTO playlists (name, description, playlistcreationdate, private) VALUES ('Like', 'Eduardo Rodrigues Liked Songs', CURRENT_DATE, TRUE);
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('eduardo.ff.rodrigues@gmail.com', 1);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (1, 1);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (1, 10);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (1, 5);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (1, 3);

INSERT INTO users (emaillogin, name, password, admin) VALUES ('luis.r.afonso@gmail.com', 'Luis Afonso', crypt('vai-esta2', gen_salt('bf', 8)), TRUE);
INSERT INTO playlists (name, description, playlistcreationdate, private) VALUES ('Like', 'Luis Afonso Liked Songs', CURRENT_DATE, TRUE);
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('luis.r.afonso@gmail.com', 2);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (2, 7);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (2, 15);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (2, 9);

INSERT INTO users (emaillogin, name, password, admin) VALUES ('alguem@gmail.com', 'Generis', crypt('123', gen_salt('bf', 8)), FALSE);
INSERT INTO playlists (name, description, playlistcreationdate, private) VALUES ('Like', 'Generis Liked Songs', CURRENT_DATE, FALSE);
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('alguem@gmail.com', 3);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (3, 2);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (3, 11);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (3, 6);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (3, 4);

INSERT INTO users (emaillogin, name, password, admin) VALUES ('secure_user@gmail.com', 'Not telling', crypt('easy', gen_salt('bf', 8)), FALSE);
INSERT INTO playlists (name, description, playlistcreationdate, private) VALUES ('Like', 'Not telling Liked Songs', CURRENT_DATE, TRUE);
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('secure_user@gmail.com', 4);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (4, 1);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (4, 10);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (4, 5);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (4, 3);

INSERT INTO playlists (name, description, playlistcreationdate, private) VALUES ('Like', 'Telling Liked Songs', CURRENT_DATE, FALSE);
INSERT INTO users_playlists (users_emaillogin, playlists_id) VALUES ('secure_user@gmail.com', 5);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (5, 1);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (5, 10);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (5, 5);
INSERT INTO playlists_songs (playlists_id, songs_id) VALUES (5, 3);
-- END Insert users

-- Try to insert American Idiot album, need publisher id and artist id --
INSERT INTO albums (name, publishers_id)
SELECT 'American Idiot', publishers.id
FROM publishers
WHERE publishers.name = 'Reprise';

INSERT INTO artist_albums (artist_id, albums_id)
SELECT artist.id, albums.id
FROM artist, albums
WHERE artist.name = 'Green Day' and albums.name = 'American Idiot';

INSERT INTO artist_albums (artist_id, albums_id)
SELECT artist.id, albums.id
FROM artist, albums
WHERE artist.name = 'Jinjer' and albums.name = 'American Idiot';

INSERT INTO genres_albums (genres_name, albums_id)
SELECT genres.name, albums.id
FROM genres, albums
WHERE (genres.name = 'Pop Punk' or genres.name = 'Alternative Rock') and albums.id = (SELECT albums.id FROM albums WHERE albums.name = 'American Idiot');
--END insert Cloud Factory album--

-- Insert lyrics (Burnout)
INSERT INTO lyrics (lyricstext, songs_id)
VALUES      (
    'I declare I don''t care no more
    I''m burning up and out and growing bored
    In my smoked-out boring room
    
    My hair is shagging in my eyes
    Dragging my feet to hit the street tonight
    To drive along these shit town lights
    
    I''m not growing up
    I''m just burning out
    And I stepped in line to walk amongst the dead
    
    Apathy has rained on me
    And now I''m feeling like a soggy dream
    So close to drowning but I don''t mind
    
    I''ve lived inside this mental cave
    Throw my emotions in the grave
    Hell, who needs them anyway?
    
    I''m not growing up
    I''m just burning out
    And I stepped in line to walk amongst the dead
    
    I''m not growing up
    I''m just burning out
    And I stepped in line to walk amongst the dead, dead
    
    I''m not growing up
    I''m just burning out
    And I stepped in line to walk amongst the dead
    
    I''m not growing up
    I''m just burning out
    And I stepped in line to walk amongst the dead, dead',
    1)
;
-- END Insert lyrics (Burnout)

-- Insert lyrics (Having a Blast)
INSERT INTO lyrics (lyricstext, songs_id)
VALUES      (
    'I''m taking all you down with me
    Explosives duct-taped to my spine
    Nothing''s gonna change my mind
    
    I won''t listen to anyone''s last words
    There''s nothing left for you to say
    Soon you''ll be dead anyway
    
    Well no one here is getting out alive
    This time I''ve really lost my mind
    And I don''t care
    So close your eyes
    And kiss yourself goodbye
    And think about the times you spent
    And what they''ve meant
    
    It''s nothing
    To me it''s nothing
    To me it''s nothing
    To me it''s nothing
    
    I''m losing all my happiness
    The happiness you pinned on me
    Loneliness still comforts me
    
    My anger dwells inside of me
    I''m taking it all out on you
    And all the shit you put me through
    
    Well no one here is getting out alive
    This time I''ve really lost my mind
    And I don''t care
    So close your eyes
    And kiss yourself goodbye
    And think about the times you spent
    And what they''ve meant
    
    To me it''s nothing
    To me it''s nothing
    To me it''s nothing
    To me it''s nothing
    
    Do you ever think back to another time?
    Does it bring you so down that you thought you lost your mind?
    
    Do you ever want to lead a long trail of destruction
    And mow down any bullshit that confronts you?
    
    Do you ever build up all the small things in your head
    To make one problem that adds up to nothing?
    
    To me it''s nothing
    To me it''s nothing
    To me it''s nothing',
    2)
;
-- END Insert lyrics (Having a Blast)