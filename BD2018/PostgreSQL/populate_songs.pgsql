-- Burnout --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Burnout', albums.id, TIME '00:02:07'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Burnout';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Burnout';
-- END Burnout --

-- Having a Blast --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Having a Blast', albums.id, TIME '00:02:44'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Having a Blast';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Having a Blast';
-- END Having a Blast --

-- Chump --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Chump', albums.id, TIME '00:02:54'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Chump';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Chump';
-- END Chump --

-- Longview --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Longview', albums.id, TIME '00:03:59'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Longview';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Longview';
-- END Longview --

-- Welcome to Paradise --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Welcome to Paradise', albums.id, TIME '00:03:44'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Welcome to Paradise';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Welcome to Paradise';
-- END Welcome to Paradise --

-- Pulling Teeth --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Pulling Teeth', albums.id, TIME '00:02:31'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Pulling Teeth';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Pulling Teeth';
-- END Pulling Teeth --

-- Basket Case --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Basket Case', albums.id, TIME '00:03:01'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Basket Case';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Basket Case';
-- END Basket Case --

-- She --
INSERT INTO songs (name, albums_id, duration)
SELECT 'She', albums.id, TIME '00:02:14'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'She';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'She';
-- END She --


-- Sassafras Roots --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Sassafras Roots', albums.id, TIME '00:02:37'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Sassafras Roots';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Sassafras Roots';
-- END Sassafras Roots --


-- When I come Around --
INSERT INTO songs (name, albums_id, duration)
SELECT 'When I come Around', albums.id, TIME '00:02:58'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'When I come Around';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'When I come Around';
-- END When I come Around --


-- Coming Clean --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Coming Clean', albums.id, TIME '00:01:34'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Coming Clean';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Coming Clean';
-- END Coming Clean --

-- Emenius Sleepus --
INSERT INTO songs (name, albums_id, duration)
SELECT 'Emenius Sleepus', albums.id, TIME '00:01:43'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'Emenius Sleepus';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'Emenius Sleepus';
-- END Emenius Sleepus --


-- In the End --
INSERT INTO songs (name, albums_id, duration)
SELECT 'In the End', albums.id, TIME '00:01:46'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'In the End';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'In the End';
-- END In the End --

-- F.O.D. --
INSERT INTO songs (name, albums_id, duration)
SELECT 'F.O.D.', albums.id, TIME '00:05:46'
FROM albums
WHERE albums.name = 'Dookie';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Green Day' and songs.name = 'F.O.D.';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and songs.name = 'F.O.D.';
-- END F.O.D. --


-- Outlander--
INSERT INTO songs (name, albums_id, duration)
SELECT 'Outlander', albums.id, TIME '00:03:56'
FROM albums
WHERE albums.name = 'Cloud Factory';

INSERT INTO artist_songs (artist_id, songs_id)
SELECT artist.id, songs.id
FROM artist, songs
WHERE artist.name = 'Jinjer' and songs.name = 'Outlander';

--I just considered all alternative indie rock --
INSERT INTO genres_songs (genres_name, songs_id)
SELECT genres.name, songs.id
FROM genres, songs
WHERE (genres.name = 'Metal' or genres.name = 'Rock') and songs.name = 'Outlander';
-- Outlander --





