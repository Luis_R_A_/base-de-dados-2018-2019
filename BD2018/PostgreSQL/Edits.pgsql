-- e.x.y -> editor only operations

-------------------------------- (1) ---------------------------
--      USER EDITS
----------------------------------------------------------------
-- (1.1) Edit name
UPDATE  users
SET     name = 'New Name'
WHERE   emaillogin = 'email@mail.com'
;

-- (1.2) Edit password
UPDATE  users
SET     password = crypt('New Password', gen_salt('bf', 8))
WHERE   emaillogin = 'email@mail.com'
;

-- (1.3) Edit photo

-- (e.1.1) Edit privileges
UPDATE  users
SET     admin = TRUE
WHERE   emaillogin = 'email@mail.com'
;

-- (e.1.2) Delete
DELETE
FROM    files
WHERE   users_emaillogin = 'email@mail.com';

UPDATE  users
SET     emaillogin = 'DELETED_' || emaillogin,
        admin = FALSE,
        password = NULL
WHERE   emaillogin = 'email@mail.com'
;

-------------------------------- (2) ---------------------------
--      PLAYLIST EDITS
----------------------------------------------------------------
-- (2.1) Create
INSERT  INTO playlists (name, description, playlistcreationdate, private) 
        VALUES ('Playlist Name', 'Playlist Description', CURRENT_DATE, TRUE)
;

-- (2.2) Delete
DELETE
FROM    playlists
WHERE   id = 1;

-- (2.3) Add song
INSERT  INTO playlists_songs (playlists_id, songs_id)
        VALUES (1, 3)
;

-- (2.4) Remove song
DELETE
FROM    playlists_songs
WHERE   playlists_id = 1 AND
        songs_id = 3
;

-- (2.5) Share
UPDATE  playlists
SET     private = FALSE
WHERE   id = 1
;

-- (2.6) Unshare
UPDATE  playlists
SET     private = TRUE
WHERE   id = 1
;

-- (2.7) Edit name
UPDATE  playlists
SET     name = 'New Name'
WHERE   id = 1
;

-- (2.8) Edit description
UPDATE  playlists
SET     description = 'Really liked songs?'
WHERE   id = 1
;

-------------------------------- (3) ---------------------------
--      FILE EDITS
----------------------------------------------------------------
-- (3.2) Upload
INSERT  INTO files (users_emaillogin, songfilename, private)
        VALUES ('email@mail.com', 'file path', TRUE)
;

-- (3.3) Download
-- (3.4) Share
UPDATE  files
SET     private = FALSE
WHERE   users_emaillogin = 'email@mail.com' AND
        songs_id = 1
;

-- (3.5) Unshare
UPDATE  files
SET     private = FALSE
WHERE   users_emaillogin = 'email@mail.com' AND
        songs_id = 1
;

-- (3.6) Delete
DELETE
FROM    files f
WHERE   users_emaillogin = 'email@mail.com' AND
        songs_id = 1
;

-------------------------------- (4) ---------------------------
--      COMMENT EDITS
----------------------------------------------------------------
-- (4.1) Comment song
INSERT  INTO comments (comment, rating, commentcreationdate, vote, users_emaillogin)
        VALUES ('New Song Comment', 5, CURRENT_DATE, 0, 'email@mail.com')
RETURNING id;

INSERT  INTO songs_comments (comments_id, songs_id)
        VALUES (4, 1)
;

-- (4.2) Comment album
INSERT  INTO comments (comment, rating, commentcreationdate, vote, users_emaillogin)
        VALUES ('New Album Comment', 5, CURRENT_DATE, 0, 'email@mail.com');

INSERT  INTO album_comments (comments_id, albums_id)
        VALUES (1, 2)
;

-- (4.3) Delete
DELETE
FROM    comments
WHERE   id = 1
;

-- (4.4) Positive vote
UPDATE  comments
SET     vote = vote + 1
WHERE   id = 2
;

-- (4.5) Negative vote
UPDATE  comments
SET     vote =  CASE WHEN vote = 0 THEN 0
                    ELSE vote - 1
                END
WHERE   id = 2
;

-- (4.6) Edit comment
UPDATE  comments
SET     comment = 'New Comment'
WHERE   id = 1
;

-- (4.7) Edit rating
UPDATE  comments
SET     rating = 5
WHERE   id = 1
;

-------------------------------- (5) ---------------------------
--      CONCERT EDITS
----------------------------------------------------------------
-- (e.5.1) Create
INSERT  INTO concerts (name, location, concertdate)
        VALUES ('Concert Name', 'City', DATE '2019-05-02')
;

-- (e.5.2) Delete
DELETE
FROM    concerts
WHERE   id = 1
;

-- (e.5.3) Add artist
INSERT  INTO concerts_artist (concerts_id, artist_id)
        VALUES (1, 2)
;

-- (e.5.4) Remove artist
DELETE
FROM    concerts_artist
WHERE   concerts_id = 1 AND
        artist_id = 2
;


-------------------------------- (6) ---------------------------
--      LYRIC EDITS
----------------------------------------------------------------
-- (e.6.1) Add
INSERT INTO lyrics (lyricstext, songs_id)
VALUES      ('Very short music x20' ,1)
;

-- (e.6.2) Remove
DELETE
FROM    lyrics
WHERE   songs_id = 1
;

-------------------------------- (7) ---------------------------
--      SONG EDITS
----------------------------------------------------------------
-- (e.7.1) Create
INSERT  INTO songs (name, albums_id, songreleasedate, duration)
        VALUES ('Song Name', 1, DATE '2014-05-23', TIME '02:37')
;

-- (e.7.2) Delete
DELETE
FROM    songs
WHERE   id = 16
;

-- (e.7.3) Edit name
UPDATE  songs
SET     name = 'New Name'
WHERE   id = 1
;

-- (e.7.4) Edit release date
UPDATE  songs
SET     songreleasedate = DATE '2014-06-24'
WHERE   id = 1
;

-- (e.7.5) Edit duration
UPDATE  songs
SET     duration = TIME '02:35'
WHERE   id = 1
;

-- (e.7.6) Edit album
UPDATE  songs
SET     albums_id = 2
WHERE   id = 1
;

-- (e.7.7) Add artist
INSERT  INTO artist_songs (artist_id, songs_id)
        VALUES (3103, 16)
;

-- (e.7.8) Remove artist
DELETE
FROM    artist_songs
WHERE   artist_id = 3103 AND
        songs_id = 16
;

-- (e.7.9) Add genre
INSERT  INTO genres_songs (genres_name, songs_id)
        VALUES ('Alternative', 16)
;

-- (e.7.10) Remove genre
DELETE
FROM    genres_songs
WHERE   genres_name = 'Alternative' AND
        songs_id = 1
;

-------------------------------- (8) ---------------------------
--      ALBUM EDITS
----------------------------------------------------------------
-- (e.8.1) Create
INSERT  INTO albums (name, albumcreationdate, publishers_id)
        VALUES ('Album Name', DATE '2015-06-24', 2)
;

-- (e.8.2) Delete
DELETE
FROM    albums
WHERE   id = 1;
;

-- (e.8.3) Edit name
UPDATE  albums
SET     name = 'New name'
WHERE   id = 1
;

-- (e.8.4) Edit release date
UPDATE  albums
SET     albumcreationdate = DATE '2015-06-24'
WHERE   id = 1
;

-- (e.8.5) Edit description
UPDATE  albums
SET     description = 'New Description'
WHERE   id = 1
;

-- (e.8.6) Edit photo
UPDATE  albums
SET     photofilename = 'photo.png'
WHERE   id = 1
;

-- (e.8.7) Edit publisher
UPDATE  albums
SET     publishers_id = 2
WHERE   id = 1
;

-- (e.8.8) Add song
UPDATE  songs
SET     albums_id = 1
WHERE   id = 16
;

-- (e.8.9) Remove song
UPDATE  songs
SET     albums_id = 0
WHERE   id = 16
;

-- (e.8.10) Add artist
INSERT  INTO artist_albums (artist_id, albums_id)
        VALUES (3103, 1)
;

-- (e.8.11) Remove artist
DELETE
FROM    artist_albums
WHERE   artist_id = 3103 AND
        albums_id = 1
;

-- (e.8.12) Add genre
INSERT  INTO genres_albums (genres_name, albums_id)
        VALUES ('Alternative', 1)
;

-- (e.8.13) Remove genre
DELETE
FROM    genres_albums
WHERE   genres_name = 'Alternative' AND
        albums_id = 1
;

-------------------------------- (9) ---------------------------
--      ARTIST EDITS
----------------------------------------------------------------
-- (e.9.1) Create
INSERT  INTO artist (name, tipo)
        VALUES ('New Artist','artist')
;

-- (e.9.2) Delete
-- (e.9.3) Edit photo
UPDATE  artist
SET     photofilename = 'photo.png'
WHERE   id = 1
;

-- (e.9.4) Edit name
UPDATE  artist
SET     name = 'New Name'
WHERE   id = 1
;

-- (e.9.5) Edit musician birthdate
UPDATE  musician
SET     birthdate = DATE '2014-05-3'
--SET     nationality = 'Mexican'
WHERE   artist_id = 1
;

-- (e.9.6) Edit musician nationality
UPDATE  musician
SET     nationality = 'Mexican'
WHERE   artist_id = 1
;

-- (e.9.7) Edit composer birthdate
UPDATE  composer
SET     birthdate = DATE '2014-05-3'
WHERE   artist_id = 1
;

-- (e.9.8) Edit composer nationality
UPDATE  composer
SET     nationality = 'Mexican'
WHERE   artist_id = 1
;

-- (e.9.9) Edit band creation date
UPDATE  band
SET     groupcreationdate = DATE '2014-05-3'
WHERE   artist_id = 1
;

-- (e.9.9) Edit band story
UPDATE  band
SET     story = 'Created when they nothing better to do'
WHERE   artist_id = 1
;
