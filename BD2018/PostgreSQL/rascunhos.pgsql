SELECT a.id, a.name 
FROM publishers a
WHERE a.name = '123';

-- To show which musicians are in which band --
select b.name "band", m.name "musician"
from artist b, artist m, band_musician
where b.id = band_musician.band_artist_id and m.id = band_musician.musician_artist_id
