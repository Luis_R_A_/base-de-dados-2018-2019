insert into users(emaillogin, name, password, admin)
values('eduardo_secure@gmail.com', 'Eduardo', crypt('1234', gen_salt('bf', 8)),True);

insert into users(emaillogin, name, password, admin)
values('luis_secure@gmail.com', 'Luis', crypt('1234', gen_salt('bf', 8)),True);

insert into users(emaillogin, name, password, admin)
values('lr', 'Luis', crypt('123', gen_salt('bf', 8)),True);
