INSERT INTO artist (name, tipo) VALUES ('Billie Joe Armstrong', 'musician');
INSERT INTO artist (name, tipo) VALUES ('Mike Dirnt', 'musician');
INSERT INTO artist (name, tipo) VALUES ('Tré Cool', 'musician');

INSERT INTO artist (name, tipo) VALUES ('Tatiana Shmailyuk', 'musician');
INSERT INTO artist (name, tipo) VALUES ('Roman Ibramkhalilov', 'musician');
INSERT INTO artist (name, tipo) VALUES ('Eugene Abdiukhanov', 'musician');
INSERT INTO artist (name, tipo) VALUES ('Vladislav Ulasevich', 'musician');

--insert musicians in musicians table--
INSERT INTO musician (artist_id)
SELECT artist.id
FROM artist
WHERE artist.tipo = 'musician';


INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Green Day' and band.artist_id = a.id)  and  (b.name = 'Billie Joe Armstrong' and musician.artist_id = b.id);
INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Green Day' and band.artist_id = a.id)  and  (b.name = 'Mike Dirnt' and musician.artist_id = b.id);
INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Green Day' and band.artist_id = a.id)  and  (b.name = 'Tré Cool' and musician.artist_id = b.id);

INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Jinjer' and band.artist_id = a.id)  and  (b.name = 'Tatiana Shmailyuk' and musician.artist_id = b.id);
INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Jinjer' and band.artist_id = a.id)  and  (b.name = 'Roman Ibramkhalilov' and musician.artist_id = b.id);
INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Jinjer' and band.artist_id = a.id)  and  (b.name = 'Eugene Abdiukhanov' and musician.artist_id = b.id);
INSERT INTO band_musician (band_artist_id, musician_artist_id)
SELECT band.artist_id, musician.artist_id
FROM band, musician, artist a, artist b
WHERE (a.name = 'Jinjer' and band.artist_id = a.id)  and  (b.name = 'Vladislav Ulasevich' and musician.artist_id = b.id);
