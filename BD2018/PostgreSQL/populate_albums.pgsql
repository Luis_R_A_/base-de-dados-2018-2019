
-- Try to insert dookie album, need publisher id and artist id --
INSERT INTO albums (name, albumcreationdate, publishers_id)
SELECT 'Dookie', DATE '1994-02-1', publishers.id
FROM publishers
WHERE publishers.name = 'Reprise';

INSERT INTO artist_albums (artist_id, albums_id)
SELECT artist.id, albums.id
FROM artist, albums
WHERE artist.name = 'Green Day' and albums.name = 'Dookie';

INSERT INTO genres_albums (genres_name, albums_id)
SELECT genres.name, albums.id
FROM genres, albums
WHERE (genres.name = 'Alternative' or genres.name = 'Indie Rock') and albums.id = (SELECT albums.id FROM albums, artist WHERE artist.name = 'Green Day' and albums.name = 'Dookie');
--END insert dookie album--

-- Try to insert Cloud Factory album, need publisher id and artist id --
INSERT INTO albums (name, albumcreationdate, publishers_id)
SELECT 'Cloud Factory', DATE '2014-05-1', publishers.id
FROM publishers
WHERE publishers.name = 'The Leaders Records';

INSERT INTO artist_albums (artist_id, albums_id)
SELECT artist.id, albums.id
FROM artist, albums
WHERE artist.name = 'Jinjer' and albums.name = 'Cloud Factory';

INSERT INTO genres_albums (genres_name, albums_id)
SELECT genres.name, albums.id
FROM genres, albums
WHERE (genres.name = 'Metal' or genres.name = 'Rock') and albums.id = (SELECT albums.id FROM albums, artist WHERE artist.name = 'Jinjer' and albums.name = 'Cloud Factory');
--END insert Cloud Factory album--
