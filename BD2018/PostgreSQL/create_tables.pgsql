CREATE EXTENSION pgcrypto;

CREATE TABLE genres (
	name VARCHAR(40) UNIQUE NOT NULL,
	PRIMARY KEY(name)
);

CREATE TABLE songs (
	id		 		BIGSERIAL,
	name		 	VARCHAR(512),
	songreleasedate DATE,
	duration	 	TIME,
	albums_id	 	BIGINT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE comments (
	id			 		BIGSERIAL,
	comment		 		VARCHAR(512),
	rating		 		INTEGER NOT NULL,
	commentcreationdate DATE NOT NULL,
	vote		 		BIGINT NOT NULL,
	users_emaillogin	VARCHAR(512) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE playlists (
	id			 			BIGSERIAL,
	name		 			VARCHAR(512) NOT NULL,
	description		 		VARCHAR(512),
	playlistcreationdate 	DATE NOT NULL,
	private		 			BOOL NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE lyrics (
	lyricsText	VARCHAR,
	songs_id 	BIGSERIAL,
	PRIMARY KEY(songs_id)
);

CREATE TABLE musician (
	artist_id	 	BIGSERIAL,
	birthdate	 	DATE,
	nationality 	VARCHAR(512),
	PRIMARY KEY(artist_id)
);

CREATE TABLE albums (
	id		 			BIGSERIAL,
	name		 		VARCHAR(512) NOT NULL,
	albumcreationdate 	DATE,
	description	 		VARCHAR(512),
	photo		 		BYTEA,
	photofilename	 	VARCHAR(512),
	publishers_id	 	BIGINT NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE artist (
	id		 		BIGSERIAL,
	photo	 		BYTEA,
	photofilename 	VARCHAR(512),
	name		 	VARCHAR(512),
	tipo		 	VARCHAR(512) NOT NULL,
	PRIMARY KEY(id)
);

CREATE TABLE concerts (
	id		 		BIGSERIAL,
	name	 		VARCHAR(512) NOT NULL,
	location	 	VARCHAR(512),
	concertdate 	DATE,
	PRIMARY KEY(id)
);

CREATE TABLE users (
	emaillogin 	VARCHAR(512),
	name	 	VARCHAR(512),
	password	VARCHAR(512) NOT NULL,
	photo	 	BYTEA,
	admin	 	BOOL NOT NULL,
	PRIMARY KEY(emaillogin)
);

CREATE TABLE album_comments (
	comments_id 	BIGSERIAL,
	albums_id	 	BIGINT NOT NULL,
	PRIMARY KEY(comments_id)
);

CREATE TABLE songs_comments (
	comments_id 	BIGSERIAL,
	songs_id	 	BIGINT NOT NULL,
	PRIMARY KEY(comments_id)
);

CREATE TABLE composer (
	artist_id	 	BIGSERIAL,
	birthdate	 	DATE,
	nationality 	VARCHAR(512),
	PRIMARY KEY(artist_id)
);

CREATE TABLE band (
	artist_id	 		BIGSERIAL,
	groupcreationdate 	DATE,
	story		 		VARCHAR(512),
	PRIMARY KEY(artist_id)
);

CREATE TABLE publishers (
	id	 	BIGSERIAL,
	name 	VARCHAR(512),
	PRIMARY KEY(id)
);

CREATE TABLE files (
	users_emaillogin 	VARCHAR(512),
	song		 		BYTEA,
	songfilename	 	VARCHAR(512) NOT NULL,
	private		 		BOOL,
	songs_id	 		BIGINT NOT NULL,
	PRIMARY KEY(users_emaillogin, songs_id)
);

CREATE TABLE band_musician (
	band_artist_id	 	BIGSERIAL,
	musician_artist_id 	BIGSERIAL,
	PRIMARY KEY(band_artist_id,musician_artist_id)
);

CREATE TABLE users_playlists (
	users_emaillogin VARCHAR(512),
	playlists_id	 BIGSERIAL,
	PRIMARY KEY(users_emaillogin,playlists_id)
);

CREATE TABLE playlists_songs (
	playlists_id 	BIGSERIAL,
	songs_id	 	BIGSERIAL,
	PRIMARY KEY(songs_id,playlists_id)
);

CREATE TABLE concerts_artist (
	concerts_id 	BIGSERIAL,
	artist_id	 	BIGSERIAL,
	PRIMARY KEY(concerts_id,artist_id)
);

CREATE TABLE artist_albums (
	artist_id 	BIGSERIAL,
	albums_id 	BIGSERIAL,
	PRIMARY KEY(albums_id,artist_id)
);

CREATE TABLE artist_songs (
	artist_id 	BIGSERIAL,
	songs_id	BIGSERIAL,
	PRIMARY KEY(songs_id,artist_id)
);

CREATE TABLE genres_songs (
	genres_name 	VARCHAR(40),
	songs_id	 	BIGSERIAL,
	PRIMARY KEY(songs_id,genres_name)
);

CREATE TABLE genres_albums (
	genres_name 	VARCHAR(40),
	albums_id	 	BIGSERIAL,
	PRIMARY KEY(albums_id,genres_name)
);

ALTER TABLE songs ADD CONSTRAINT songs_fk1 FOREIGN KEY (albums_id) REFERENCES albums(id) ON DELETE CASCADE;
ALTER TABLE comments ADD CONSTRAINT comments_fk1 FOREIGN KEY (users_emaillogin) REFERENCES users(emaillogin);
ALTER TABLE lyrics ADD CONSTRAINT lyrics_fk1 FOREIGN KEY (songs_id) REFERENCES songs(id) ON DELETE CASCADE;
ALTER TABLE musician ADD CONSTRAINT musician_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE;
ALTER TABLE albums ADD CONSTRAINT albums_fk1 FOREIGN KEY (publishers_id) REFERENCES publishers(id);
ALTER TABLE album_comments ADD CONSTRAINT album_comments_fk1 FOREIGN KEY (albums_id) REFERENCES albums(id) ON DELETE CASCADE;
ALTER TABLE album_comments ADD CONSTRAINT album_comments_fk2 FOREIGN KEY (comments_id) REFERENCES comments(id) ON DELETE CASCADE;
ALTER TABLE songs_comments ADD CONSTRAINT songs_comments_fk1 FOREIGN KEY (songs_id) REFERENCES songs(id) ON DELETE CASCADE;
ALTER TABLE songs_comments ADD CONSTRAINT songs_comments_fk2 FOREIGN KEY (comments_id) REFERENCES comments(id) ON DELETE CASCADE;
ALTER TABLE composer ADD CONSTRAINT composer_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE;
ALTER TABLE band ADD CONSTRAINT band_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE;
ALTER TABLE files ADD CONSTRAINT files_fk1 FOREIGN KEY (users_emaillogin) REFERENCES users(emaillogin);
ALTER TABLE files ADD CONSTRAINT files_fk2 FOREIGN KEY (songs_id) REFERENCES songs(id);
ALTER TABLE band_musician ADD CONSTRAINT band_musician_fk1 FOREIGN KEY (band_artist_id) REFERENCES band(artist_id) ON DELETE CASCADE;
ALTER TABLE band_musician ADD CONSTRAINT band_musician_fk2 FOREIGN KEY (musician_artist_id) REFERENCES musician(artist_id) ON DELETE CASCADE;
ALTER TABLE users_playlists ADD CONSTRAINT users_playlists_fk1 FOREIGN KEY (users_emaillogin) REFERENCES users(emaillogin);
ALTER TABLE users_playlists ADD CONSTRAINT users_playlists_fk2 FOREIGN KEY (playlists_id) REFERENCES playlists(id) ON DELETE CASCADE;
ALTER TABLE playlists_songs ADD CONSTRAINT playlists_songs_fk1 FOREIGN KEY (playlists_id) REFERENCES playlists(id) ON DELETE CASCADE;
ALTER TABLE playlists_songs ADD CONSTRAINT playlists_songs_fk2 FOREIGN KEY (songs_id) REFERENCES songs(id) ON DELETE CASCADE;
ALTER TABLE concerts_artist ADD CONSTRAINT concerts_artist_fk1 FOREIGN KEY (concerts_id) REFERENCES concerts(id) ON DELETE CASCADE;
ALTER TABLE concerts_artist ADD CONSTRAINT concerts_artist_fk2 FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE;
ALTER TABLE artist_albums ADD CONSTRAINT artist_albums_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id)  ON DELETE CASCADE;
ALTER TABLE artist_albums ADD CONSTRAINT artist_albums_fk2 FOREIGN KEY (albums_id) REFERENCES albums(id)  ON DELETE CASCADE;
ALTER TABLE artist_songs ADD CONSTRAINT artist_songs_fk1 FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE;
ALTER TABLE artist_songs ADD CONSTRAINT artist_songs_fk2 FOREIGN KEY (songs_id) REFERENCES songs(id) ON DELETE CASCADE;
ALTER TABLE genres_songs ADD CONSTRAINT genres_songs_fk1 FOREIGN KEY (genres_name) REFERENCES genres(name) ON DELETE CASCADE;
ALTER TABLE genres_songs ADD CONSTRAINT genres_songs_fk2 FOREIGN KEY (songs_id) REFERENCES songs(id) ON DELETE CASCADE;
ALTER TABLE genres_albums ADD CONSTRAINT genres_albums_fk1 FOREIGN KEY (genres_name) REFERENCES genres(name)  ON DELETE CASCADE;
ALTER TABLE genres_albums ADD CONSTRAINT genres_albums_fk2 FOREIGN KEY (albums_id) REFERENCES albums(id)  ON DELETE CASCADE;
