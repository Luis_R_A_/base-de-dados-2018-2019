-------------------------------- (1) ---------------------------
--      USERS
----------------------------------------------------------------
-- (1.1) Search Users
SELECT  emaillogin,
        name
FROM    users
WHERE   LOWER(emaillogin) LIKE LOWER('%%') OR
        LOWER(name) LIKE LOWER('%%')
;

-- (1.2) Open User
SELECT  photo           AS "Photo",
        emaillogin      AS "Email",
        name            AS "Name"
FROM    users
WHERE   emaillogin = 'email@mail.com'
;

SELECT  pl.id,
        pl.name         AS "Playlist"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   (pl.private = FALSE     AND u.emaillogin = 'email@mail.com')
;

-------------------------------- (2) ---------------------------
--      LOGIN
----------------------------------------------------------------
-- (2.1) Registration
INSERT  INTO users (emaillogin, name, password, admin)
        VALUES ('email@mail.com', 'Name Surname', crypt('password', gen_salt('bf', 8)), FALSE);
INSERT  INTO playlists (name, description, playlistcreationdate, private) 
        VALUES ('Like', 'Name Surname''s Liked Songs', CURRENT_DATE, TRUE)
RETURNING id;
INSERT  INTO users_playlists (users_emaillogin, playlists_id) 
        VALUES ('email@mail.com', 4)
;

-- (2.2) Login
SELECT  u.emaillogin
FROM    users u
WHERE   LOWER(u.emaillogin) = LOWER('email@mail.com') AND
        u.password = crypt('password', u.password)
;

-------------------------------- (3) ---------------------------
--      ALBUMS
----------------------------------------------------------------

-- (3.1) Search Album
SELECT  CASE    WHEN min(al.albumcreationdate) IS NOT NULL
                        THEN min(al.albumcreationdate)
                ELSE DATE '1500-1-1'
        END,
        CASE    WHEN max(al.albumcreationdate) IS NOT NULL
                        THEN max(al.albumcreationdate)
                ELSE CURRENT_DATE
        END
FROM    albums al;

SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM            albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN    genres_albums g_al ON g_al.albums_id = al.id
LEFT JOIN    genres g           ON g_al.genres_name = g.name
JOIN    publishers p            ON al.publishers_id = p.id
JOIN    artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN    artist ar               ON ar_al.artist_id = ar.id
WHERE   UPPER(al.name)          LIKE UPPER('%%') AND
        UPPER(p.name)           LIKE UPPER('%%') AND
        UPPER(g.name)           LIKE UPPER('%%') AND
        UPPER(ar.name)          LIKE UPPER('%%') AND
        (CASE   WHEN al.albumcreationdate IS NOT NULL
                        THEN al.albumcreationdate
                ELSE NULL
        END BETWEEN DATE '1990-1-1' AND DATE '2015-1-1') OR
        al.albumcreationdate IS NULL
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- (3.2) Open Genre Albums
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM            albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN genres_albums g_al    ON g_al.albums_id = al.id
LEFT JOIN genres g              ON g_al.genres_name = g.name
JOIN    publishers p            ON al.publishers_id = p.id
JOIN    artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN    artist ar               ON ar_al.artist_id = ar.id
WHERE   g.name = 'Alternative'
GROUP BY al.id,
        al.photo,
        al.name
ORDER BY al.name
;

-- (3.3) Open Album
SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct g.name),'/') AS "Genre",
        al.description                                  AS "Description",
        p.name                                          AS "Publisher",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        al.albumcreationdate                            AS "Release Date", 
        AVG(c.rating)                                   AS "Rating"
FROM            albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN    genres_albums g_al ON g_al.albums_id = al.id
LEFT JOIN    genres g           ON g_al.genres_name = g.name
JOIN    publishers p            ON al.publishers_id = p.id
JOIN    artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN    artist ar               ON ar_al.artist_id = ar.id
WHERE   al.id = 1
GROUP BY al.id,
        al.photo,
        al.name,
        al.description,
        p.name,
        al.albumcreationdate
ORDER BY al.name;

SELECT  s.id,
        s.name          AS "Name",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
WHERE           s.albums_id = 1
GROUP BY        s.id
;

-------------------------------- (4) ---------------------------
--      ARTISTS
----------------------------------------------------------------
-- (4.1) Search Artists
SELECT  a.id,
        a.name,
        a.tipo
FROM artist a
WHERE UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- (4.2) Filter Artists
    -- (4.2.1) Filter Bands
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'band' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

    -- (4.2.1) Filter Composers
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'composer' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

    -- (4.2.1) Filter Musicians
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo
FROM artist a
WHERE a.tipo = 'musician' AND UPPER(a.name) LIKE UPPER('%%')
ORDER BY a.name
;

-- (4.3) Open Artist
SELECT  a.id,
        a.photo,
        a.name,
        a.tipo,
        CASE WHEN LOWER(a.tipo) = LOWER('band')
                THEN   (SELECT  b.groupcreationdate
                        FROM    band b
                        WHERE   b.artist_id = a.id)
             WHEN LOWER(a.tipo) = LOWER('musician')
                THEN   (SELECT  m.birthdate
                        FROM    musician m
                        WHERE   m.artist_id = a.id)
             ELSE      (SELECT  c.birthdate
                        FROM    composer c
                        WHERE   c.artist_id = a.id)
        END,
        CASE WHEN LOWER(a.tipo) = LOWER('band')
                THEN   (SELECT  b.story
                        FROM    band b
                        WHERE   b.artist_id = a.id)
             WHEN LOWER(a.tipo) = LOWER('musician')
                THEN   (SELECT  m.nationality
                        FROM    musician m
                        WHERE   m.artist_id = a.id)
             ELSE      (SELECT  c.nationality
                        FROM    composer c
                        WHERE   c.artist_id = a.id)
        END
FROM artist a
LEFT JOIN band b ON b.artist_id = a.id
LEFT JOIN musician m ON m.artist_id = a.id
LEFT JOIN composer c ON c.artist_id = a.id
WHERE a.id = 3096;

SELECT  al.id,
        al.photo                                        AS "Artwork",
        al.name                                         AS "Name",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artist",
        AVG(c.rating)                                   AS "Rating"
FROM            albums al
LEFT JOIN       album_comments a_c      ON a_c.albums_id = al.id
LEFT JOIN       comments c              ON a_c.comments_id = c.id
LEFT JOIN    genres_albums g_al ON g_al.albums_id = al.id
LEFT JOIN    genres g           ON g_al.genres_name = g.name
JOIN    publishers p            ON al.publishers_id = p.id
JOIN    artist_albums ar_al     ON ar_al.albums_id = al.id
JOIN    artist ar               ON ar_al.artist_id = ar.id
WHERE   ar.id = 3096
GROUP BY al.id,
        al.photo,
        al.name,
        al.description,
        p.name,
        al.albumcreationdate
ORDER BY al.name
;

-------------------------------- (5) ---------------------------
--      SONGS
----------------------------------------------------------------
-- (5.1) Search Song
-- Init (returns min and max)
SELECT  CASE    WHEN min(s.songreleasedate) IS NOT NULL 
                        THEN min(s.songreleasedate)
                WHEN min(al.albumcreationdate) IS NOT NULL
                        THEN min(al.albumcreationdate)
                ELSE DATE '1500-1-1'
        END,
        CASE    WHEN max(s.songreleasedate) IS NOT NULL 
                        THEN max(s.songreleasedate)
                WHEN max(al.albumcreationdate) IS NOT NULL
                        THEN max(al.albumcreationdate)
                ELSE CURRENT_DATE
        END
FROM    songs s, albums al;
-- Search 
SELECT  s.id,
        s.name                  AS "Name",
        al.name                 AS "Album",
        s.duration              AS "Duration",
        AVG(c.rating)           AS "Rating"
FROM            songs s
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
LEFT JOIN       genres_songs g_s   ON g_s.songs_id = s.id
LEFT JOIN       genres g           ON g_s.genres_name = g.name
LEFT JOIN       artist_songs ar_s  ON ar_s.songs_id = s.id
LEFT JOIN       artist ar          ON ar_s.artist_id = ar.id
WHERE           UPPER(s.name)           LIKE UPPER('%c%') AND
        UPPER(ar.name)          LIKE UPPER('%%') AND
        UPPER(g.name)           LIKE UPPER('%%') AND
        CASE WHEN s.songreleasedate IS NOT NULL
                THEN s.songreleasedate 
                ELSE    CASE WHEN al.albumcreationdate IS NOT NULL
                                THEN al.albumcreationdate
                                ELSE now()
                        END
        END BETWEEN DATE '1990-1-1' AND DATE '2015-1-1'
GROUP BY s.id,
        s.name,
        al.name,
        s.duration
;

-- (5.2) Open User Songs (Songs with files)
SELECT  s.id,
        s.name                  AS "Name",
        al.name                 AS "Album",
        s.duration              AS "Duration",
        f.songfilename          AS "File",
        AVG(c.rating)           AS "Rating"
FROM            songs s
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       files f                 ON f.songs_id = s.id
JOIN            users u                 ON (f.users_emaillogin = u.emaillogin OR
                                           f.private = FALSE)
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
LEFT JOIN       genres_songs g_s        ON g_s.songs_id = s.id
LEFT JOIN       genres g                ON g_s.genres_name = g.name
LEFT JOIN       artist_songs ar_s       ON ar_s.songs_id = s.id
LEFT JOIN       artist ar               ON ar_s.artist_id = ar.id
WHERE   u.emaillogin = 'email@mail.com' AND
        UPPER(s.name)           LIKE UPPER('%%') AND
        UPPER(ar.name)          LIKE UPPER('%%') AND
        UPPER(g.name)           LIKE UPPER('%%') AND
        CASE WHEN s.songreleasedate IS NOT NULL
                THEN s.songreleasedate 
             WHEN al.albumcreationdate IS NOT NULL
                THEN al.albumcreationdate
             ELSE now()
        END BETWEEN DATE '1990-1-1' AND DATE '2015-1-1'
GROUP BY s.id,
        s.name,
        al.name,
        s.duration,
        f.songfilename
;

-- (5.3) Open Song
SELECT  s.id,
        s.name                                          AS "Name",
        al.name                                         AS "Album",
        s.duration                                      AS "Duration",
        AVG(c.rating)                                   AS "Rating",
        s.songreleasedate                               AS "Release Date",
        array_to_string(array_agg(distinct g.name),'/') AS "Genre",
        array_to_string(array_agg(distinct ar.name),'/') AS "Artists",
        CASE    WHEN (l.lyricstext) IS NOT NULL THEN l.lyricstext
                ELSE ''
        END                                             AS "Lyric",
        CASE    WHEN    (f.private = FALSE AND 
                        f.users_emaillogin = 'email@mail.com')
                        THEN f.songfilename
                ELSE 'No file'
        END                                             AS "Files"
FROM    songs s
JOIN            albums al               ON s.albums_id = al.id
LEFT JOIN       files f                 ON s.id = f.songs_id
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
LEFT JOIN       genres_songs g_s        ON g_s.songs_id = s.id
LEFT JOIN       genres g                ON g_s.genres_name = g.name
LEFT JOIN       artist_songs ar_s       ON ar_s.songs_id = s.id
LEFT JOIN       artist ar               ON ar_s.artist_id = ar.id
LEFT JOIN       lyrics l                ON l.songs_id = s.id
WHERE           s.id = 1
GROUP BY s.id,
        s.name,
        al.name,
        s.duration,
        l.lyricstext,
        f.private,
        f.users_emaillogin
;

-------------------------------- (6) ---------------------------
--      PLAYLISTS
----------------------------------------------------------------
-- (6.1) Search Playlist
SELECT  pl.id,
        pl.name         AS "Name",
        u.emaillogin    AS "Owner"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   (pl.private = FALSE     OR u.emaillogin = 'email@mail.com') AND
        LOWER(pl.name) LIKE LOWER('%%')
;

-- (6.2) Open Playlist
SELECT  pl.id,
        pl.name                 AS "Name",
        pl.description          AS "Description",
        pl.playlistcreationdate AS "Creation Date",
        u.emaillogin            AS "Owner"
FROM    playlists pl
JOIN    users_playlists u_pl    ON u_pl.playlists_id = pl.id
JOIN    users u                 ON u.emaillogin = u_pl.users_emaillogin
WHERE   pl.id = 4;

SELECT  s.id,
        s.name          AS "Name",
        al.name         AS "Album",
        s.duration      AS "Duration",
        AVG(c.rating)   AS "Rating"
FROM    songs s
LEFT JOIN       songs_comments s_c      ON s_c.songs_id = s.id
LEFT JOIN       comments c              ON s_c.comments_id = c.id
JOIN            albums al               ON al.id = s.albums_id
JOIN            playlists_songs pl_s    ON pl_s.songs_id = s.id
JOIN            playlists pl            ON pl_s.playlists_id = pl.id
WHERE           pl.id = 4
GROUP BY        s.id,
                al.name
;

-------------------------------- (7) ---------------------------
--      COMMENTS
----------------------------------------------------------------
-- (7.1) Open Album Comments
SELECT  c.id,
        u.emaillogin            AS "User",
        c.comment               AS "Comment",
        c.commentcreationdate   AS "Comment Date",
        c.rating                AS "Rating",
        c.vote                  AS "Votes"
FROM    comments c
JOIN    users u                 ON u.emaillogin = c.users_emaillogin
JOIN    album_comments al_c     ON al_c.comments_id = c.id
JOIN    albums al               ON al.id = al_c.albums_id
WHERE   al.id = 1
;

-- (7.2) Open Song Comments
SELECT  c.id,
        u.emaillogin            AS "User",
        c.comment               AS "Comment",
        c.commentcreationdate   AS "Comment Date",
        c.rating                AS "Rating",
        c.vote                  AS "Votes"
FROM    comments c
JOIN    users u                 ON u.emaillogin = c.users_emaillogin
JOIN    songs_comments s_c      ON s_c.comments_id = c.id
JOIN    songs s                 ON s.id = s_c.songs_id
WHERE   s.id = 1
;

-------------------------------- (8) ---------------------------
--      CONCERTS
----------------------------------------------------------------
-- (8.1) Search Concert
SELECT  c.id,
        c.name          AS "Concert",
        c.location      AS "Local",
        c.concertdate   AS "Date"
FROM    concerts c
LEFT JOIN concerts_artist c_a   ON c_a.concerts_id = c.id
JOIN    artist a                ON a.id = c_a.artist_id
WHERE   LOWER(a.name)           LIKE LOWER('%%') AND
        LOWER(c.name)           LIKE LOWER('%%') AND
        LOWER(c.location)       LIKE LOWER('%%') AND
        c.concertdate BETWEEN DATE '1990-1-1' AND DATE '2030-1-1'
;

-- (8.2) Open Concert
SELECT  c.id,
        c.name          AS "Concert",
        c.location      AS "Local",
        c.concertdate   AS "Date"
FROM    concerts c
WHERE   c.id = 1;

SELECT  a.id,
        a.photo         AS "Photo",
        a.name          AS "Name"
FROM    artist a
JOIN    concerts_artist c_a     ON c_a.artist_id = a.id
JOIN    concerts c              ON c_a.concerts_id = c.id
WHERE   c.id = 1
;

-- (8.3) Open Concerts from artist
SELECT  c.id,
        c.name          AS "Concert",
        c.location      AS "Local",
        c.concertdate   AS "Date"
FROM    concerts c
LEFT JOIN concerts_artist c_a   ON c_a.concerts_id = c.id
JOIN    artist a                ON a.id = c_a.artist_id
WHERE   a.id = 1
;

-------------------------------- (9) ---------------------------
--      FILE
----------------------------------------------------------------
-- (3.1) Search
SELECT  s.name          AS "Song",
        f.songfilename  AS "File",
        u.emaillogin    AS "Owner"
FROM    files f
JOIN    songs s     ON f.songs_id = s.id
JOIN    users u     ON f.users_emaillogin = u.emaillogin
WHERE   (f.private = FALSE OR
        u.emaillogin = 'email@mail.com') AND
        LOWER(s.name) LIKE LOWER('%%')
;
