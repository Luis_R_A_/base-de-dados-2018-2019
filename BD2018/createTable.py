# Pedro Furtado (pnf@dei.uc.pt), Bases de Dados 2018/2019,
# baseado no exemplo em
# http://www.postgresqltutorial.com/postgresql-python
import psycopg2
import sys

def create_tables():

    connection = None
    try:

        connection = psycopg2.connect(host="localhost",database="DropMusic", user="postgres", password="postgres")
        cursor = connection.cursor()
        sql_file = open('PostgreSQL/create_tables.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating publishers")
        sql_file = open('PostgreSQL/populate_publishers.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating genres")
        sql_file = open('PostgreSQL/populate_genres.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating bands")
        sql_file = open('PostgreSQL/populate_bands.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating musician")
        sql_file = open('PostgreSQL/populate_musician.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating composers")
        sql_file = open('PostgreSQL/populate_composers.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating albums")
        sql_file = open('PostgreSQL/populate_albums.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating songs")
        sql_file = open('PostgreSQL/populate_songs.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating editors")
        sql_file = open('PostgreSQL/populate_editors.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()

        print("Populating test values")
        sql_file = open('PostgreSQL/populate_to_test.pgsql', 'r')
        cursor.execute(sql_file.read())
        sql_file.close()


        # close communication with the PostgreSQL database server
        cursor.close()
        # commit the changes
        connection.commit()


        #albumObject = c_album()

        #albumObject.create(connection, 'American Idiot', 'Reprise', 'Green Day', ['Pop Punk', 'Alternative Rock'], albumCreationDate='2014-09-20', description="Very good album")




    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if connection is not None:
            connection.close()


if __name__ == '__main__':
    create_tables()
    input("press enter to continue")
