class c_songElement():

    def __init__(self):
        self.id = None
        self.name  = None
        self.songReleaseDate = None
        self.duration = None
        self.genres = []
        self.listArtists = []
        self.albumName = None
        self.rating = None
        self.lyrics = None
        pass

    #order:
    def getDetails(self, details):
        #assume ID, name, album name, duration, rating, release date, genre, artists, lyrics
        self.id = details[0]
        self.name = details[1]
        self.albumName = details[2]
        self.duration = details[3]
        self.rating = details[4]
        self.songReleaseDate = details[5]
        genres = details[6].split('/')
        for g in genres:
            self.genres.append(g)
        artists = details[7].split('/')
        for a in artists:
            self.listArtists.append(a)
        self.lyrics = details[8]

    def getGeneral(self, general):
        #assumes: ID, name, album name, duration, rating
        self.id = general[0]
        self.name = general[1]
        self.albumName = general[2]
        self.duration = general[3]
        self.rating = general[4]

    def getInAlbum(self, inAlbum):
        #assumes: ID, name, duration, rating

        self.id = inAlbum[0]
        self.name = inAlbum[1]
        self.duration =inAlbum[2]
        self.rating = inAlbum[3]

    def printDetails(self):
        print("ID:", self.id, "|name:", self.name, "|album name:", self.albumName, "|duration:", self.duration, "|rating:", self.rating,
              "|release date:", self.songReleaseDate, "|genre:", self.genres, "|artists:", self.listArtists, "|lyrics:", self.lyrics)
    def printGeneral(self):
        print("ID:", self.id, "|name:", self.name, "|album name:", self.albumName, "|duration:", self.duration, "|rating:", self.rating)
    def printInAlbum(self):
        print("ID:", self.id, "|name:", self.name, "|duration:", self.duration, "|rating:", self.rating)

    def decodeDetails(self):
        stringArtist = self.listArtists[0]
        for x in self.listArtists[1:]:
            stringArtist += "\n " + x

        stringGenres =  self.genres[0]
        for x in self.genres[1:]:
            stringGenres+= "\n " + x
        toprint = "ID: \n {}\n\n" \
                  "Name: \n {}\n\n" \
                  "Song Name: \n {}\n\n" \
                  "Artists: \n {}\n\n" \
                  "Duration: \n {} \n\n" \
                  "Song release date: \n {}\n\n" \
                  "Genres: \n {}\n\n" \
                  "Rating: \n {}\n\n".format(self.id, self.name, self.albumName, stringArtist, self.duration.strftime("%M:%S"), self.songReleaseDate,  stringGenres, self.rating)
        return toprint

    def getLyrics(self):
        return self.lyrics

    def decodeInAlbum(self):

        toprint = "{:7}|{:30}|{:10}|{:5}".format(self.id, self.name, self.duration.strftime("%M:%S"),self.rating)

        return toprint
    def headerInAlbum(self):
        toPrint = "{:7}|{:30}|{:10}|{:5}".format("ID", "Name", "Duration", 'Rating')
        return toPrint

    def decodeGeneral(self):
        toprint = "{:7}|{:30}|{:30}|{:10}|{:5}".format(self.id, self.name, self.albumName, self.duration.strftime("%M:%S"),self.rating)

        return toprint

    def headerGeneral(self):
        toPrint = "{:7}|{:30}|{:30}|{:10}|{:5}".format("ID", "Name", "Album", "Duration", 'Rating')
        return toPrint

    def getID(self):
        return self.id


class c_song():

    def __init__(self):

        self.sqlSearchSongsByGenre = """SELECT  s.id,
                                                s.name          AS "Name",
                                                al.name         AS "Album",
                                                s.duration      AS "Duration",
                                                'Stars'         AS "Rating"
                                        FROM    songs s
                                        JOIN    albums al               ON s.albums_id = al.id
                                        LEFT JOIN    genres_songs g_s   ON g_s.songs_id = s.id
                                        LEFT JOIN    genres g           ON g_s.genres_name = g.name
                                        WHERE   UPPER(g.name)           LIKE UPPER(%s)
                                        GROUP BY s.id,
                                                s.name,
                                                al.name,
                                                s.duration
                                        ORDER BY s.id,
                                                al.name
                                        ;"""
        #order: artist name key
        self.sqlSearchSongsByArtist = """SELECT  s.id,
                                                    s.name          AS "Name",
                                                    al.name         AS "Album",
                                                    s.duration      AS "Duration",
                                                    'Stars'         AS "Rating"
                                            FROM    songs s
                                            JOIN    albums al               ON s.albums_id = al.id
                                            LEFT JOIN    artist_songs ar_s  ON ar_s.songs_id = s.id
                                            LEFT JOIN    artist ar          ON ar_s.artist_id = ar.id
                                            WHERE   UPPER(ar.name)          LIKE UPPER(%s)
                                            ;"""
        #order: song name key
        self.sqlSearchSongsByName = """SELECT  s.id,
                                                s.name          AS "Name",
                                                al.name         AS "Album",
                                                s.duration      AS "Duration",
                                                'Stars'         AS "Rating"
                                        FROM    songs s
                                        JOIN    albums al       ON s.albums_id = al.id
                                        WHERE   UPPER(s.name)   LIKE UPPER(%s)
                                        ;"""
        #order: song id
        self.sqlGetSongInfo = """SELECT  s.id,
                                        s.name AS "Name",
                                        al.name AS "Album",
                                        s.duration AS "Duration",
                                        'Stars' AS "Rating",
                                        s.songreleasedate AS "Release Date",
                                        array_to_string(array_agg(distinct g.name),'/') AS "Genre",
                                        array_to_string(array_agg(distinct ar.name),'/') AS "Artists",
                                        CASE WHEN (l.lyricstext) IS NOT NULL THEN l.lyricstext
                                            ELSE ''
                                        END AS "Lyric",
                                        'Files' AS "Files"
                                FROM    songs s
                                JOIN    albums al ON s.albums_id = al.id
                                LEFT JOIN    genres_songs g_s ON g_s.songs_id = s.id
                                LEFT JOIN    genres g ON g_s.genres_name = g.name
                                LEFT JOIN    artist_songs ar_s ON ar_s.songs_id = s.id
                                LEFT JOIN    artist ar ON ar_s.artist_id = ar.id
                                LEFT JOIN lyrics l ON l.songs_id = s.id
                                WHERE   s.id = (%s)
                                GROUP BY s.id,
                                        s.name,
                                        al.name,
                                        s.duration,
                                        l.lyricstext
                                ;"""

        self.sqlMinMaxDate = """SELECT  CASE WHEN min(s.songreleasedate) IS NOT NULL 
                                                THEN min(s.songreleasedate)
                                                ELSE    CASE WHEN min(al.albumcreationdate) IS NOT NULL
                                                                THEN min(al.albumcreationdate)
                                                                ELSE DATE '1500-1-1'
                                                        END
                                        END,
                                        CASE WHEN max(s.songreleasedate) IS NOT NULL 
                                                THEN max(s.songreleasedate)
                                                ELSE     CASE WHEN max(al.albumcreationdate) IS NOT NULL
                                                                THEN max(al.albumcreationdate)
                                                                ELSE now()
                                                        END
                                        END
                                FROM    songs s, albums al
                                ;"""

        #order: minDate and maxDate
        self.sqlSearchSongsByMinMaxDate = """SELECT  s.id,
                                                    s.name          AS "Name",
                                                    al.name         AS "Album",
                                                    s.duration      AS "Duration",
                                                    'Stars'         AS "Rating"
                                            FROM    songs s
                                            JOIN    albums al       ON s.albums_id = al.id
                                            WHERE   CASE WHEN s.songreleasedate IS NOT NULL
                                                            THEN s.songreleasedate 
                                                            ELSE    CASE WHEN al.albumcreationdate IS NOT NULL
                                                                            THEN al.albumcreationdate
                                                                            ELSE now()
                                                                    END
                                                    END BETWEEN DATE (%s) AND DATE (%s)
                                            ;"""

        #order: lyrics, song id
        self.sqlInsertLyric = """INSERT INTO lyrics (lyricsText, songs_id)
                        SELECT (%s), (%s)
                        RETURNING songs_id;
                        """
        #order: lyrics, song id
        self.sqlUpdateLyric = """UPDATE lyrics SET lyricsText = (%s)
                                WHERE songs_id = (%s)"""

        #order: song id
        self.sqlCheckIfLyricExists = """ SELECT lyrics.songs_id
                FROM lyrics
                WHERE songs_id = (%s)"""

    def insertLyrics(self, connection, id, lyric):
        print("Insert new lyric into song")
        cursor = connection.cursor()

        cursor.execute(self.sqlCheckIfLyricExists, [id])
        idCheck = cursor.fetchone()

        if idCheck:
            cursor.execute(self.sqlUpdateLyric, [lyric, id])
        else:
            print([lyric, id])
            cursor.execute(self.sqlInsertLyric, [lyric, id])


        connection.commit()

        cursor.close()



    def getMinMaxDate(self,connection):
        print("Getting list of songs")
        cursor = connection.cursor()
        cursor.execute(self.sqlMinMaxDate)


        listSongs = cursor.fetchall()
        #print(listSongs)
        cursor.close()
        return listSongs
        pass
    def headerGeneral(self):
        return c_songElement().headerGeneral()
    def headerInAlbum(self):
        return c_songElement().headerInAlbum()

    def searchSongsByName(self, connection, searchKey):
        print("Getting list of songs")
        encodedName = '%'+searchKey+'%'
        cursor = connection.cursor()
        cursor.execute(self.sqlSearchSongsByName, [encodedName])


        listSongs = cursor.fetchall()
        connection.commit()


        listOfSongs = []

        for x in listSongs:
            newElement = c_songElement()
            newElement.getGeneral(x)
            listOfSongs.append(newElement)


        cursor.close()

        return listOfSongs

    def searchSongsByArtist(self, connection, searchKey):
        print("Getting list of songs")
        encodedName = '%'+searchKey+'%'
        cursor = connection.cursor()
        cursor.execute(self.sqlSearchSongsByArtist, [encodedName])


        listSongs = cursor.fetchall()
        connection.commit()


        listOfSongs = []

        for x in listSongs:
            newElement = c_songElement()
            newElement.getGeneral(x)
            listOfSongs.append(newElement)


        cursor.close()

        return listOfSongs

    def searchSongsByGenre(self, connection, searchKey):
        print("Searching list of songs by genre")
        encodedName = '%'+searchKey+'%'
        cursor = connection.cursor()
        cursor.execute(self.sqlSearchSongsByGenre, [encodedName])


        listSongs = cursor.fetchall()
        connection.commit()


        listOfSongs = []

        for x in listSongs:
            newElement = c_songElement()
            newElement.getGeneral(x)
            listOfSongs.append(newElement)


        cursor.close()

        return listOfSongs

    def searchSongsByMinMaxDate(self, connection, minDate, maxDate):
        print("Getting list of songs")

        s_minDate = "{}-{}-{}".format(minDate.year,minDate.month, minDate.day)
        s_maxDate = "{}-{}-{}".format(maxDate.year,maxDate.month, maxDate.day)
        cursor = connection.cursor()
        cursor.execute(self.sqlSearchSongsByMinMaxDate, [s_minDate, s_maxDate])


        listSongs = cursor.fetchall()
        connection.commit()


        listOfSongs = []

        for x in listSongs:
            newElement = c_songElement()
            newElement.getGeneral(x)
            listOfSongs.append(newElement)


        cursor.close()

        return listOfSongs

    def getSongInfo(self, connection, id):
        print("Getting song details")
        cursor = connection.cursor()
        cursor.execute(self.sqlGetSongInfo, [id] )


        songInfo = cursor.fetchall()
        connection.commit()


        newElement = None
        listOfAlbums = []

        for x in songInfo:
            newElement = c_songElement()
            newElement.getDetails(x)
            listOfAlbums.append(newElement)



        cursor.close()

        #newElement.printDetails()
        return newElement

