



class c_login():

    def __init__(self):
        self.sessionIsAdmin = False
        self.currentEmail = None
        self.currentPhoto = None
        self.currentName = None


        #order: email, password
        self.sqlLogin = """SELECT  u.admin, u.photo
                            FROM    users u
                            WHERE   LOWER(u.emaillogin) = LOWER((%s)) AND
                                    u.password = crypt((%s), u.password)
                            ;"""

        self.sqlCheckUsernameExists = """SELECT name FROM users WHERE emaillogin = (%s)"""

        #order:  email, name, password
        self.sqlRegisterNewUser = """INSERT INTO users(emaillogin, name, password, admin)
                                        VALUES((%s), (%s), crypt((%s), gen_salt('bf', 8)),False);"""



        self.sqlMakeAdmin = """UPDATE users 
                                SET admin = True
                                WHERE emaillogin = (%s) RETURNING emaillogin"""

        self.sqlRemoveAdmin = """UPDATE users 
                                SET admin = False
                                WHERE emaillogin = (%s) RETURNING emaillogin"""


        self.sqlCheckIfAdmin = """SELECT admin FROM users WHERE emaillogin = (%s) """

        pass



    def checkIfAdmin(self, connection, email):


        print("Checking if user is admin")
        cursor = connection.cursor()
        cursor.execute(self.sqlCheckIfAdmin, ( [email]) )
        userCheck  = cursor.fetchall()
        print(userCheck)
        if not userCheck:
            print("user does not exist")
        elif(userCheck[0][0] == True):
            print("user is admin")
        elif(userCheck[0][0] == False):
            print("user is not admin")

    def makeAdmin(self, connection, email):
        print("Making new user admin")
        if not self.sessionIsAdmin:
            print("Current user not admin, can't do that")
            return -1


        cursor = connection.cursor()
        print(email)
        cursor.execute(self.sqlMakeAdmin, ( [email]) )
        userCheck  = cursor.fetchall()


        connection.commit()
        cursor.close()

        if not userCheck:
            print("user name did not exist")
            return "Does not exist"


    def removeAdmin(self, connection, email):
        print("removing user admin")
        if not self.sessionIsAdmin:
            print("Current user not admin, can't do that")
            return -1



        cursor = connection.cursor()
        cursor.execute(self.sqlRemoveAdmin, ( [email]) )

        connection.commit()


        userCheck  = cursor.fetchall()
        cursor.close()
        if not userCheck:
            print("user name did not exist")
            return "Does not exist"


    def register(self, connection, email, password, name):
        print("Checking if user already exists")
        cursor = connection.cursor()
        cursor.execute(self.sqlCheckUsernameExists, ( [email]) )
        userCheck  = cursor.fetchall()
        if userCheck:
            return "Already exists"

        print("registering user")
        cursor.execute(self.sqlRegisterNewUser, (email, name, password) )

        connection.commit()
        cursor.close()

        return None



    def login(self, connection, email, password):
        print("Checking login")
        cursor = connection.cursor()
        cursor.execute(self.sqlLogin, (email, password) )
        self.connection = connection

        loginCheck = cursor.fetchall()

        cursor.close()

        if loginCheck:
            self.currentEmail = email
            self.currentPhoto = loginCheck[0][1]
            if loginCheck[0][0]:
                self.sessionIsAdmin = True
                return "admin"
            else:
                self.sessionIsAdmin = False
                return "user"
        else:
            return None


    def logOut(self):
        self.currentPhoto = None
        self.currentEmail = None
        self.sessionIsAdmin = False


    def changePhoto(self):
        pass
